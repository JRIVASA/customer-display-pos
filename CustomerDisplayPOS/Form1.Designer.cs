﻿namespace CustomerDisplayPOS
{
    partial class CustomerDisplayPOS
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CustomerDisplayPOS));
            this.TimerUpdateDatas = new System.Windows.Forms.Timer(this.components);
            this.Frame_Abajo = new System.Windows.Forms.Panel();
            this.txtTestFontSize1 = new System.Windows.Forms.TextBox();
            this.txtTestLen1 = new System.Windows.Forms.TextBox();
            this.Panel2 = new System.Windows.Forms.Panel();
            this.Peso = new System.Windows.Forms.Label();
            this.Lbl_Peso = new System.Windows.Forms.Label();
            this.Panel3 = new System.Windows.Forms.Panel();
            this.lblTotAlt2 = new System.Windows.Forms.Label();
            this.Total = new System.Windows.Forms.Label();
            this.lblTotAlt1 = new System.Windows.Forms.Label();
            this.Lbl_Total = new System.Windows.Forms.Label();
            this.Grd_Fact = new System.Windows.Forms.DataGridView();
            this.Panel1 = new System.Windows.Forms.Panel();
            this.MontoIGTF = new System.Windows.Forms.Label();
            this.lbl_IGTF = new System.Windows.Forms.Label();
            this.Donacion = new System.Windows.Forms.Label();
            this.lbl_Donacion = new System.Windows.Forms.Label();
            this.Descuento = new System.Windows.Forms.Label();
            this.lbl_Descuento = new System.Windows.Forms.Label();
            this.LogoBz = new System.Windows.Forms.PictureBox();
            this.Impuesto = new System.Windows.Forms.Label();
            this.Subtotal = new System.Windows.Forms.Label();
            this.Lbl_Impuesto = new System.Windows.Forms.Label();
            this.Lbl_Subtotal = new System.Windows.Forms.Label();
            this.Lbl_Cedula = new System.Windows.Forms.Label();
            this.Lbl_Nombre = new System.Windows.Forms.Label();
            this.Frame_Superior = new System.Windows.Forms.Panel();
            this.lblGranTotalAlt2 = new System.Windows.Forms.Label();
            this.lblGranTotalAlt1 = new System.Windows.Forms.Label();
            this.lblGranTotal = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.Surt_plus = new System.Windows.Forms.PictureBox();
            this.Precio_plus = new System.Windows.Forms.PictureBox();
            this.Limp_plus = new System.Windows.Forms.PictureBox();
            this.Aten_plus = new System.Windows.Forms.PictureBox();
            this.msgGracias = new System.Windows.Forms.PictureBox();
            this.Aten = new System.Windows.Forms.PictureBox();
            this.Surt = new System.Windows.Forms.PictureBox();
            this.Precio = new System.Windows.Forms.PictureBox();
            this.Limp = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.MsgCaras = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.PicCancel = new System.Windows.Forms.PictureBox();
            this.PicOK = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.PicCancelPlus = new System.Windows.Forms.PictureBox();
            this.PicOKPlus = new System.Windows.Forms.PictureBox();
            this.TimerBanner = new System.Windows.Forms.Timer(this.components);
            this.FoodLogo = new System.Windows.Forms.PictureBox();
            this.Frame_Abajo.SuspendLayout();
            this.Panel2.SuspendLayout();
            this.Panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Fact)).BeginInit();
            this.Panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LogoBz)).BeginInit();
            this.Frame_Superior.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Surt_plus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Precio_plus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Limp_plus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Aten_plus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.msgGracias)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Aten)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Surt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Precio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Limp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicOK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCancelPlus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicOKPlus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FoodLogo)).BeginInit();
            this.SuspendLayout();
            // 
            // TimerUpdateDatas
            // 
            this.TimerUpdateDatas.Enabled = true;
            this.TimerUpdateDatas.Interval = 3000;
            this.TimerUpdateDatas.Tick += new System.EventHandler(this.TimerUpdateDates_Tick);
            // 
            // Frame_Abajo
            // 
            this.Frame_Abajo.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.Frame_Abajo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Frame_Abajo.Controls.Add(this.txtTestFontSize1);
            this.Frame_Abajo.Controls.Add(this.txtTestLen1);
            this.Frame_Abajo.Controls.Add(this.Panel2);
            this.Frame_Abajo.Controls.Add(this.Panel3);
            this.Frame_Abajo.Controls.Add(this.Grd_Fact);
            this.Frame_Abajo.Controls.Add(this.Panel1);
            this.Frame_Abajo.Location = new System.Drawing.Point(0, 375);
            this.Frame_Abajo.Name = "Frame_Abajo";
            this.Frame_Abajo.Size = new System.Drawing.Size(1366, 391);
            this.Frame_Abajo.TabIndex = 10;
            this.Frame_Abajo.Paint += new System.Windows.Forms.PaintEventHandler(this.Frame_Abajo_Paint);
            // 
            // txtTestFontSize1
            // 
            this.txtTestFontSize1.BackColor = System.Drawing.Color.Black;
            this.txtTestFontSize1.ForeColor = System.Drawing.SystemColors.Window;
            this.txtTestFontSize1.Location = new System.Drawing.Point(594, 289);
            this.txtTestFontSize1.Name = "txtTestFontSize1";
            this.txtTestFontSize1.Size = new System.Drawing.Size(213, 20);
            this.txtTestFontSize1.TabIndex = 20;
            this.txtTestFontSize1.Text = "28";
            this.txtTestFontSize1.Visible = false;
            this.txtTestFontSize1.TextChanged += new System.EventHandler(this.txtTestFontSize1_TextChanged);
            // 
            // txtTestLen1
            // 
            this.txtTestLen1.BackColor = System.Drawing.Color.Black;
            this.txtTestLen1.ForeColor = System.Drawing.SystemColors.Window;
            this.txtTestLen1.Location = new System.Drawing.Point(375, 289);
            this.txtTestLen1.Name = "txtTestLen1";
            this.txtTestLen1.Size = new System.Drawing.Size(213, 20);
            this.txtTestLen1.TabIndex = 19;
            this.txtTestLen1.Text = "0.00";
            this.txtTestLen1.Visible = false;
            this.txtTestLen1.TextChanged += new System.EventHandler(this.txtTestLen1_TextChanged);
            // 
            // Panel2
            // 
            this.Panel2.BackColor = System.Drawing.Color.DarkGray;
            this.Panel2.Controls.Add(this.Peso);
            this.Panel2.Controls.Add(this.Lbl_Peso);
            this.Panel2.Location = new System.Drawing.Point(1000, 336);
            this.Panel2.Name = "Panel2";
            this.Panel2.Size = new System.Drawing.Size(355, 48);
            this.Panel2.TabIndex = 12;
            // 
            // Peso
            // 
            this.Peso.AutoEllipsis = true;
            this.Peso.BackColor = System.Drawing.Color.Transparent;
            this.Peso.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Peso.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Peso.Location = new System.Drawing.Point(212, 9);
            this.Peso.Name = "Peso";
            this.Peso.Size = new System.Drawing.Size(135, 31);
            this.Peso.TabIndex = 14;
            this.Peso.Text = "Peso";
            this.Peso.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.Peso.Click += new System.EventHandler(this.Peso_Click);
            // 
            // Lbl_Peso
            // 
            this.Lbl_Peso.BackColor = System.Drawing.Color.Transparent;
            this.Lbl_Peso.Font = new System.Drawing.Font("Arial Rounded MT Bold", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lbl_Peso.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Lbl_Peso.Location = new System.Drawing.Point(14, 9);
            this.Lbl_Peso.Name = "Lbl_Peso";
            this.Lbl_Peso.Size = new System.Drawing.Size(183, 28);
            this.Lbl_Peso.TabIndex = 11;
            this.Lbl_Peso.Text = "Peso del producto";
            this.Lbl_Peso.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Panel3
            // 
            this.Panel3.BackColor = System.Drawing.Color.DimGray;
            this.Panel3.Controls.Add(this.lblTotAlt2);
            this.Panel3.Controls.Add(this.Total);
            this.Panel3.Controls.Add(this.lblTotAlt1);
            this.Panel3.Controls.Add(this.Lbl_Total);
            this.Panel3.Location = new System.Drawing.Point(1000, 245);
            this.Panel3.Name = "Panel3";
            this.Panel3.Size = new System.Drawing.Size(356, 84);
            this.Panel3.TabIndex = 18;
            // 
            // lblTotAlt2
            // 
            this.lblTotAlt2.AutoEllipsis = true;
            this.lblTotAlt2.BackColor = System.Drawing.Color.DimGray;
            this.lblTotAlt2.Font = new System.Drawing.Font("Franklin Gothic Demi", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotAlt2.ForeColor = System.Drawing.Color.White;
            this.lblTotAlt2.Location = new System.Drawing.Point(2, 8);
            this.lblTotAlt2.Margin = new System.Windows.Forms.Padding(0);
            this.lblTotAlt2.Name = "lblTotAlt2";
            this.lblTotAlt2.Size = new System.Drawing.Size(178, 38);
            this.lblTotAlt2.TabIndex = 22;
            this.lblTotAlt2.Text = "$ 9.99";
            this.lblTotAlt2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblTotAlt2.Visible = false;
            this.lblTotAlt2.TextChanged += new System.EventHandler(this.lblTotAlt2_TextChanged);
            this.lblTotAlt2.Click += new System.EventHandler(this.lblTotAlt2_Click);
            // 
            // Total
            // 
            this.Total.AutoEllipsis = true;
            this.Total.BackColor = System.Drawing.Color.DimGray;
            this.Total.Font = new System.Drawing.Font("Franklin Gothic Demi", 28F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Total.ForeColor = System.Drawing.Color.White;
            this.Total.Location = new System.Drawing.Point(0, 41);
            this.Total.Margin = new System.Windows.Forms.Padding(0);
            this.Total.Name = "Total";
            this.Total.Size = new System.Drawing.Size(351, 38);
            this.Total.TabIndex = 8;
            this.Total.Text = "B$ 999,999,999.99";
            this.Total.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.Total.TextChanged += new System.EventHandler(this.Total_TextChanged);
            this.Total.Click += new System.EventHandler(this.Total_Click_1);
            // 
            // lblTotAlt1
            // 
            this.lblTotAlt1.AutoEllipsis = true;
            this.lblTotAlt1.BackColor = System.Drawing.Color.DimGray;
            this.lblTotAlt1.Font = new System.Drawing.Font("Franklin Gothic Demi", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotAlt1.ForeColor = System.Drawing.Color.White;
            this.lblTotAlt1.Location = new System.Drawing.Point(159, 8);
            this.lblTotAlt1.Margin = new System.Windows.Forms.Padding(0);
            this.lblTotAlt1.Name = "lblTotAlt1";
            this.lblTotAlt1.Size = new System.Drawing.Size(106, 38);
            this.lblTotAlt1.TabIndex = 23;
            this.lblTotAlt1.Text = "$ 9,999.99";
            this.lblTotAlt1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblTotAlt1.Visible = false;
            this.lblTotAlt1.TextChanged += new System.EventHandler(this.lblTotAlt1_TextChanged);
            this.lblTotAlt1.Click += new System.EventHandler(this.lblTotAlt1_Click);
            // 
            // Lbl_Total
            // 
            this.Lbl_Total.Font = new System.Drawing.Font("Franklin Gothic Demi", 22F);
            this.Lbl_Total.ForeColor = System.Drawing.Color.White;
            this.Lbl_Total.Location = new System.Drawing.Point(3, 10);
            this.Lbl_Total.Name = "Lbl_Total";
            this.Lbl_Total.Size = new System.Drawing.Size(346, 32);
            this.Lbl_Total.TabIndex = 7;
            this.Lbl_Total.Text = "Total:";
            this.Lbl_Total.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.Lbl_Total.Click += new System.EventHandler(this.Lbl_Total_Click);
            // 
            // Grd_Fact
            // 
            this.Grd_Fact.AllowUserToAddRows = false;
            this.Grd_Fact.AllowUserToDeleteRows = false;
            this.Grd_Fact.AllowUserToResizeColumns = false;
            this.Grd_Fact.AllowUserToResizeRows = false;
            this.Grd_Fact.BackgroundColor = System.Drawing.Color.White;
            this.Grd_Fact.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_Fact.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.Grd_Fact.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Arial Narrow", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.Firebrick;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Grd_Fact.DefaultCellStyle = dataGridViewCellStyle2;
            this.Grd_Fact.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.Grd_Fact.EnableHeadersVisualStyles = false;
            this.Grd_Fact.Location = new System.Drawing.Point(7, 3);
            this.Grd_Fact.MultiSelect = false;
            this.Grd_Fact.Name = "Grd_Fact";
            this.Grd_Fact.ReadOnly = true;
            this.Grd_Fact.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Grd_Fact.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_Fact.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.Grd_Fact.RowHeadersVisible = false;
            this.Grd_Fact.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Grd_Fact.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Grd_Fact.ShowCellErrors = false;
            this.Grd_Fact.ShowCellToolTips = false;
            this.Grd_Fact.ShowEditingIcon = false;
            this.Grd_Fact.ShowRowErrors = false;
            this.Grd_Fact.Size = new System.Drawing.Size(987, 381);
            this.Grd_Fact.TabIndex = 13;
            this.Grd_Fact.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.Grd_Fact_CellContentClick);
            // 
            // Panel1
            // 
            this.Panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.Panel1.Controls.Add(this.FoodLogo);
            this.Panel1.Controls.Add(this.MontoIGTF);
            this.Panel1.Controls.Add(this.lbl_IGTF);
            this.Panel1.Controls.Add(this.Donacion);
            this.Panel1.Controls.Add(this.lbl_Donacion);
            this.Panel1.Controls.Add(this.Descuento);
            this.Panel1.Controls.Add(this.lbl_Descuento);
            this.Panel1.Controls.Add(this.LogoBz);
            this.Panel1.Controls.Add(this.Impuesto);
            this.Panel1.Controls.Add(this.Subtotal);
            this.Panel1.Controls.Add(this.Lbl_Impuesto);
            this.Panel1.Controls.Add(this.Lbl_Subtotal);
            this.Panel1.Controls.Add(this.Lbl_Cedula);
            this.Panel1.Controls.Add(this.Lbl_Nombre);
            this.Panel1.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Panel1.Location = new System.Drawing.Point(1000, 3);
            this.Panel1.Name = "Panel1";
            this.Panel1.Size = new System.Drawing.Size(356, 236);
            this.Panel1.TabIndex = 11;
            // 
            // MontoIGTF
            // 
            this.MontoIGTF.AutoEllipsis = true;
            this.MontoIGTF.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MontoIGTF.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.MontoIGTF.Location = new System.Drawing.Point(161, 208);
            this.MontoIGTF.Name = "MontoIGTF";
            this.MontoIGTF.Size = new System.Drawing.Size(187, 25);
            this.MontoIGTF.TabIndex = 24;
            this.MontoIGTF.Text = "999,999,999.99";
            this.MontoIGTF.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.MontoIGTF.Visible = false;
            this.MontoIGTF.TextChanged += new System.EventHandler(this.MontoIGTF_TextChanged);
            this.MontoIGTF.Click += new System.EventHandler(this.MontoIGTF_Click);
            // 
            // lbl_IGTF
            // 
            this.lbl_IGTF.AutoEllipsis = true;
            this.lbl_IGTF.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_IGTF.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lbl_IGTF.Location = new System.Drawing.Point(9, 208);
            this.lbl_IGTF.Name = "lbl_IGTF";
            this.lbl_IGTF.Size = new System.Drawing.Size(145, 25);
            this.lbl_IGTF.TabIndex = 23;
            this.lbl_IGTF.Text = "IGTF:";
            this.lbl_IGTF.Visible = false;
            // 
            // Donacion
            // 
            this.Donacion.AutoEllipsis = true;
            this.Donacion.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Donacion.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Donacion.Location = new System.Drawing.Point(160, 183);
            this.Donacion.Name = "Donacion";
            this.Donacion.Size = new System.Drawing.Size(187, 25);
            this.Donacion.TabIndex = 20;
            this.Donacion.Text = "999,999,999.99";
            this.Donacion.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.Donacion.Visible = false;
            this.Donacion.TextChanged += new System.EventHandler(this.Donacion_TextChanged);
            this.Donacion.Click += new System.EventHandler(this.Donacion_Click);
            // 
            // lbl_Donacion
            // 
            this.lbl_Donacion.AutoEllipsis = true;
            this.lbl_Donacion.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Donacion.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lbl_Donacion.Location = new System.Drawing.Point(8, 183);
            this.lbl_Donacion.Name = "lbl_Donacion";
            this.lbl_Donacion.Size = new System.Drawing.Size(145, 25);
            this.lbl_Donacion.TabIndex = 19;
            this.lbl_Donacion.Text = "Donación:";
            this.lbl_Donacion.Visible = false;
            // 
            // Descuento
            // 
            this.Descuento.AutoEllipsis = true;
            this.Descuento.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Descuento.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Descuento.Location = new System.Drawing.Point(160, 123);
            this.Descuento.Name = "Descuento";
            this.Descuento.Size = new System.Drawing.Size(187, 25);
            this.Descuento.TabIndex = 18;
            this.Descuento.Text = "999,999,999.99";
            this.Descuento.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.Descuento.Visible = false;
            this.Descuento.TextChanged += new System.EventHandler(this.Descuento_TextChanged);
            this.Descuento.Click += new System.EventHandler(this.Descuento_Click);
            // 
            // lbl_Descuento
            // 
            this.lbl_Descuento.AutoEllipsis = true;
            this.lbl_Descuento.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Descuento.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lbl_Descuento.Location = new System.Drawing.Point(9, 123);
            this.lbl_Descuento.Name = "lbl_Descuento";
            this.lbl_Descuento.Size = new System.Drawing.Size(145, 25);
            this.lbl_Descuento.TabIndex = 17;
            this.lbl_Descuento.Text = "Descuento:";
            this.lbl_Descuento.Visible = false;
            // 
            // LogoBz
            // 
            this.LogoBz.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("LogoBz.BackgroundImage")));
            this.LogoBz.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.LogoBz.ErrorImage = null;
            this.LogoBz.Location = new System.Drawing.Point(65, 12);
            this.LogoBz.Name = "LogoBz";
            this.LogoBz.Size = new System.Drawing.Size(236, 50);
            this.LogoBz.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.LogoBz.TabIndex = 16;
            this.LogoBz.TabStop = false;
            this.LogoBz.Click += new System.EventHandler(this.LogoBz_Click);
            // 
            // Impuesto
            // 
            this.Impuesto.AutoEllipsis = true;
            this.Impuesto.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Impuesto.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Impuesto.Location = new System.Drawing.Point(160, 155);
            this.Impuesto.Name = "Impuesto";
            this.Impuesto.Size = new System.Drawing.Size(187, 25);
            this.Impuesto.TabIndex = 15;
            this.Impuesto.Text = "999,999,999.99";
            this.Impuesto.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.Impuesto.TextChanged += new System.EventHandler(this.Impuesto_TextChanged);
            this.Impuesto.Click += new System.EventHandler(this.Impuesto_Click);
            // 
            // Subtotal
            // 
            this.Subtotal.AutoEllipsis = true;
            this.Subtotal.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Subtotal.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Subtotal.Location = new System.Drawing.Point(160, 92);
            this.Subtotal.Name = "Subtotal";
            this.Subtotal.Size = new System.Drawing.Size(187, 25);
            this.Subtotal.TabIndex = 13;
            this.Subtotal.Text = "999,999,999.99";
            this.Subtotal.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.Subtotal.TextChanged += new System.EventHandler(this.Subtotal_TextChanged);
            this.Subtotal.Click += new System.EventHandler(this.Subtotal_Click);
            // 
            // Lbl_Impuesto
            // 
            this.Lbl_Impuesto.AutoEllipsis = true;
            this.Lbl_Impuesto.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lbl_Impuesto.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Lbl_Impuesto.Location = new System.Drawing.Point(9, 153);
            this.Lbl_Impuesto.Name = "Lbl_Impuesto";
            this.Lbl_Impuesto.Size = new System.Drawing.Size(145, 25);
            this.Lbl_Impuesto.TabIndex = 12;
            this.Lbl_Impuesto.Text = "IVA:";
            // 
            // Lbl_Subtotal
            // 
            this.Lbl_Subtotal.AutoEllipsis = true;
            this.Lbl_Subtotal.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lbl_Subtotal.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Lbl_Subtotal.Location = new System.Drawing.Point(9, 90);
            this.Lbl_Subtotal.Name = "Lbl_Subtotal";
            this.Lbl_Subtotal.Size = new System.Drawing.Size(145, 25);
            this.Lbl_Subtotal.TabIndex = 10;
            this.Lbl_Subtotal.Text = "Subtotal:";
            // 
            // Lbl_Cedula
            // 
            this.Lbl_Cedula.AutoSize = true;
            this.Lbl_Cedula.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lbl_Cedula.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Lbl_Cedula.Location = new System.Drawing.Point(61, 44);
            this.Lbl_Cedula.Name = "Lbl_Cedula";
            this.Lbl_Cedula.Size = new System.Drawing.Size(97, 21);
            this.Lbl_Cedula.TabIndex = 9;
            this.Lbl_Cedula.Text = "Lbl_Cedula";
            this.Lbl_Cedula.Visible = false;
            // 
            // Lbl_Nombre
            // 
            this.Lbl_Nombre.Font = new System.Drawing.Font("Calibri", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lbl_Nombre.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Lbl_Nombre.Location = new System.Drawing.Point(59, 25);
            this.Lbl_Nombre.Name = "Lbl_Nombre";
            this.Lbl_Nombre.Size = new System.Drawing.Size(253, 40);
            this.Lbl_Nombre.TabIndex = 8;
            this.Lbl_Nombre.Text = "Lbl_Nombre";
            this.Lbl_Nombre.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Lbl_Nombre.Visible = false;
            // 
            // Frame_Superior
            // 
            this.Frame_Superior.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.Frame_Superior.BackColor = System.Drawing.Color.Transparent;
            this.Frame_Superior.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Frame_Superior.BackgroundImage")));
            this.Frame_Superior.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Frame_Superior.Controls.Add(this.lblGranTotalAlt2);
            this.Frame_Superior.Controls.Add(this.lblGranTotalAlt1);
            this.Frame_Superior.Controls.Add(this.lblGranTotal);
            this.Frame_Superior.Controls.Add(this.label5);
            this.Frame_Superior.Controls.Add(this.Surt_plus);
            this.Frame_Superior.Controls.Add(this.Precio_plus);
            this.Frame_Superior.Controls.Add(this.Limp_plus);
            this.Frame_Superior.Controls.Add(this.Aten_plus);
            this.Frame_Superior.Controls.Add(this.msgGracias);
            this.Frame_Superior.Controls.Add(this.Aten);
            this.Frame_Superior.Controls.Add(this.Surt);
            this.Frame_Superior.Controls.Add(this.Precio);
            this.Frame_Superior.Controls.Add(this.Limp);
            this.Frame_Superior.Controls.Add(this.pictureBox5);
            this.Frame_Superior.Controls.Add(this.MsgCaras);
            this.Frame_Superior.Controls.Add(this.label4);
            this.Frame_Superior.Controls.Add(this.label3);
            this.Frame_Superior.Controls.Add(this.label2);
            this.Frame_Superior.Controls.Add(this.label1);
            this.Frame_Superior.Controls.Add(this.PicCancel);
            this.Frame_Superior.Controls.Add(this.PicOK);
            this.Frame_Superior.Controls.Add(this.pictureBox4);
            this.Frame_Superior.Controls.Add(this.pictureBox3);
            this.Frame_Superior.Controls.Add(this.pictureBox2);
            this.Frame_Superior.Controls.Add(this.pictureBox1);
            this.Frame_Superior.Controls.Add(this.PicCancelPlus);
            this.Frame_Superior.Controls.Add(this.PicOKPlus);
            this.Frame_Superior.Location = new System.Drawing.Point(0, 1);
            this.Frame_Superior.Name = "Frame_Superior";
            this.Frame_Superior.Size = new System.Drawing.Size(1365, 371);
            this.Frame_Superior.TabIndex = 11;
            // 
            // lblGranTotalAlt2
            // 
            this.lblGranTotalAlt2.AutoEllipsis = true;
            this.lblGranTotalAlt2.Font = new System.Drawing.Font("Arial Rounded MT Bold", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGranTotalAlt2.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.lblGranTotalAlt2.Location = new System.Drawing.Point(778, 171);
            this.lblGranTotalAlt2.Name = "lblGranTotalAlt2";
            this.lblGranTotalAlt2.Size = new System.Drawing.Size(246, 28);
            this.lblGranTotalAlt2.TabIndex = 38;
            this.lblGranTotalAlt2.Text = "GRAN TOTAL ALT 2";
            this.lblGranTotalAlt2.Visible = false;
            this.lblGranTotalAlt2.TextChanged += new System.EventHandler(this.lblGranTotalAlt2_TextChanged);
            // 
            // lblGranTotalAlt1
            // 
            this.lblGranTotalAlt1.AutoEllipsis = true;
            this.lblGranTotalAlt1.Font = new System.Drawing.Font("Arial Rounded MT Bold", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGranTotalAlt1.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.lblGranTotalAlt1.Location = new System.Drawing.Point(778, 138);
            this.lblGranTotalAlt1.Name = "lblGranTotalAlt1";
            this.lblGranTotalAlt1.Size = new System.Drawing.Size(246, 28);
            this.lblGranTotalAlt1.TabIndex = 37;
            this.lblGranTotalAlt1.Text = "GRAN TOTAL ALT 1";
            this.lblGranTotalAlt1.Visible = false;
            this.lblGranTotalAlt1.TextChanged += new System.EventHandler(this.lblGranTotalAlt1_TextChanged);
            // 
            // lblGranTotal
            // 
            this.lblGranTotal.AutoEllipsis = true;
            this.lblGranTotal.Font = new System.Drawing.Font("Arial Rounded MT Bold", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGranTotal.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.lblGranTotal.Location = new System.Drawing.Point(824, 110);
            this.lblGranTotal.Name = "lblGranTotal";
            this.lblGranTotal.Size = new System.Drawing.Size(173, 28);
            this.lblGranTotal.TabIndex = 36;
            this.lblGranTotal.Text = "GRAN TOTAL";
            this.lblGranTotal.Visible = false;
            this.lblGranTotal.TextChanged += new System.EventHandler(this.lblGranTotal_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label5.Location = new System.Drawing.Point(558, 171);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(278, 31);
            this.label5.TabIndex = 34;
            this.label5.Text = "¿Como calificaria su";
            this.label5.Visible = false;
            // 
            // Surt_plus
            // 
            this.Surt_plus.Image = ((System.Drawing.Image)(resources.GetObject("Surt_plus.Image")));
            this.Surt_plus.Location = new System.Drawing.Point(826, 261);
            this.Surt_plus.Name = "Surt_plus";
            this.Surt_plus.Size = new System.Drawing.Size(104, 28);
            this.Surt_plus.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Surt_plus.TabIndex = 33;
            this.Surt_plus.TabStop = false;
            this.Surt_plus.Visible = false;
            // 
            // Precio_plus
            // 
            this.Precio_plus.Image = ((System.Drawing.Image)(resources.GetObject("Precio_plus.Image")));
            this.Precio_plus.Location = new System.Drawing.Point(696, 262);
            this.Precio_plus.Name = "Precio_plus";
            this.Precio_plus.Size = new System.Drawing.Size(104, 28);
            this.Precio_plus.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Precio_plus.TabIndex = 32;
            this.Precio_plus.TabStop = false;
            this.Precio_plus.Visible = false;
            // 
            // Limp_plus
            // 
            this.Limp_plus.Image = ((System.Drawing.Image)(resources.GetObject("Limp_plus.Image")));
            this.Limp_plus.Location = new System.Drawing.Point(565, 262);
            this.Limp_plus.Name = "Limp_plus";
            this.Limp_plus.Size = new System.Drawing.Size(105, 27);
            this.Limp_plus.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Limp_plus.TabIndex = 31;
            this.Limp_plus.TabStop = false;
            this.Limp_plus.Visible = false;
            // 
            // Aten_plus
            // 
            this.Aten_plus.Image = ((System.Drawing.Image)(resources.GetObject("Aten_plus.Image")));
            this.Aten_plus.Location = new System.Drawing.Point(436, 262);
            this.Aten_plus.Name = "Aten_plus";
            this.Aten_plus.Size = new System.Drawing.Size(104, 27);
            this.Aten_plus.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Aten_plus.TabIndex = 30;
            this.Aten_plus.TabStop = false;
            this.Aten_plus.Visible = false;
            // 
            // msgGracias
            // 
            this.msgGracias.Image = ((System.Drawing.Image)(resources.GetObject("msgGracias.Image")));
            this.msgGracias.Location = new System.Drawing.Point(178, 135);
            this.msgGracias.Name = "msgGracias";
            this.msgGracias.Size = new System.Drawing.Size(329, 83);
            this.msgGracias.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.msgGracias.TabIndex = 29;
            this.msgGracias.TabStop = false;
            this.msgGracias.Visible = false;
            // 
            // Aten
            // 
            this.Aten.Location = new System.Drawing.Point(435, 301);
            this.Aten.Name = "Aten";
            this.Aten.Size = new System.Drawing.Size(103, 27);
            this.Aten.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Aten.TabIndex = 28;
            this.Aten.TabStop = false;
            this.Aten.Visible = false;
            this.Aten.Click += new System.EventHandler(this.Aten_Click);
            // 
            // Surt
            // 
            this.Surt.Location = new System.Drawing.Point(826, 301);
            this.Surt.Name = "Surt";
            this.Surt.Size = new System.Drawing.Size(102, 27);
            this.Surt.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Surt.TabIndex = 27;
            this.Surt.TabStop = false;
            this.Surt.Visible = false;
            this.Surt.Click += new System.EventHandler(this.Surt_Click);
            // 
            // Precio
            // 
            this.Precio.Location = new System.Drawing.Point(696, 301);
            this.Precio.Name = "Precio";
            this.Precio.Size = new System.Drawing.Size(102, 27);
            this.Precio.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Precio.TabIndex = 26;
            this.Precio.TabStop = false;
            this.Precio.Visible = false;
            this.Precio.Click += new System.EventHandler(this.Precio_Click);
            // 
            // Limp
            // 
            this.Limp.Location = new System.Drawing.Point(565, 302);
            this.Limp.Name = "Limp";
            this.Limp.Size = new System.Drawing.Size(104, 26);
            this.Limp.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Limp.TabIndex = 25;
            this.Limp.TabStop = false;
            this.Limp.Visible = false;
            this.Limp.Click += new System.EventHandler(this.Limp_Click);
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox5.Image")));
            this.pictureBox5.Location = new System.Drawing.Point(366, 376);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(131, 40);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox5.TabIndex = 24;
            this.pictureBox5.TabStop = false;
            this.pictureBox5.Visible = false;
            // 
            // MsgCaras
            // 
            this.MsgCaras.AutoSize = true;
            this.MsgCaras.Font = new System.Drawing.Font("Arial Rounded MT Bold", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MsgCaras.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.MsgCaras.Location = new System.Drawing.Point(513, 155);
            this.MsgCaras.Name = "MsgCaras";
            this.MsgCaras.Size = new System.Drawing.Size(358, 37);
            this.MsgCaras.TabIndex = 23;
            this.MsgCaras.Text = "Gracias por su tiempo";
            this.MsgCaras.Visible = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label4.Location = new System.Drawing.Point(916, 262);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(210, 20);
            this.label4.TabIndex = 22;
            this.label4.Text = "satisfacen de nuestra tienda";
            this.label4.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label3.Location = new System.Drawing.Point(861, 227);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(276, 20);
            this.label3.TabIndex = 21;
            this.label3.Text = "Indique la o las cualidades que mas le";
            this.label3.Visible = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label2.Location = new System.Drawing.Point(959, 72);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(323, 31);
            this.label2.TabIndex = 20;
            this.label2.Text = "experiencia de compra?";
            this.label2.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label1.Location = new System.Drawing.Point(987, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(278, 31);
            this.label1.TabIndex = 19;
            this.label1.Text = "¿Como calificaria su";
            this.label1.Visible = false;
            // 
            // PicCancel
            // 
            this.PicCancel.BackColor = System.Drawing.Color.Transparent;
            this.PicCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.PicCancel.Image = ((System.Drawing.Image)(resources.GetObject("PicCancel.Image")));
            this.PicCancel.Location = new System.Drawing.Point(1371, 147);
            this.PicCancel.Name = "PicCancel";
            this.PicCancel.Size = new System.Drawing.Size(109, 110);
            this.PicCancel.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicCancel.TabIndex = 18;
            this.PicCancel.TabStop = false;
            this.PicCancel.Visible = false;
            this.PicCancel.Click += new System.EventHandler(this.PicCancel_Click);
            // 
            // PicOK
            // 
            this.PicOK.BackColor = System.Drawing.Color.Transparent;
            this.PicOK.Image = ((System.Drawing.Image)(resources.GetObject("PicOK.Image")));
            this.PicOK.Location = new System.Drawing.Point(1371, 146);
            this.PicOK.Name = "PicOK";
            this.PicOK.Size = new System.Drawing.Size(108, 112);
            this.PicOK.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicOK.TabIndex = 17;
            this.PicOK.TabStop = false;
            this.PicOK.Visible = false;
            this.PicOK.Click += new System.EventHandler(this.PicOK_Click);
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
            this.pictureBox4.Location = new System.Drawing.Point(698, 374);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(131, 40);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 16;
            this.pictureBox4.TabStop = false;
            this.pictureBox4.Visible = false;
            this.pictureBox4.Click += new System.EventHandler(this.pictureBox4_Click);
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(533, 374);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(131, 40);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 15;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.Visible = false;
            this.pictureBox3.Click += new System.EventHandler(this.pictureBox3_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(365, 372);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(131, 42);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 14;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Visible = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(866, 374);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(131, 40);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 13;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Visible = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // PicCancelPlus
            // 
            this.PicCancelPlus.Location = new System.Drawing.Point(700, 138);
            this.PicCancelPlus.Margin = new System.Windows.Forms.Padding(0);
            this.PicCancelPlus.Name = "PicCancelPlus";
            this.PicCancelPlus.Size = new System.Drawing.Size(80, 80);
            this.PicCancelPlus.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicCancelPlus.TabIndex = 4;
            this.PicCancelPlus.TabStop = false;
            this.PicCancelPlus.Visible = false;
            this.PicCancelPlus.Click += new System.EventHandler(this.PicCancelPlus_Click);
            // 
            // PicOKPlus
            // 
            this.PicOKPlus.Location = new System.Drawing.Point(581, 137);
            this.PicOKPlus.Margin = new System.Windows.Forms.Padding(0);
            this.PicOKPlus.Name = "PicOKPlus";
            this.PicOKPlus.Size = new System.Drawing.Size(80, 80);
            this.PicOKPlus.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicOKPlus.TabIndex = 2;
            this.PicOKPlus.TabStop = false;
            this.PicOKPlus.Visible = false;
            this.PicOKPlus.Click += new System.EventHandler(this.PicOKPlus_Click);
            // 
            // TimerBanner
            // 
            this.TimerBanner.Enabled = true;
            this.TimerBanner.Interval = 10000;
            this.TimerBanner.Tick += new System.EventHandler(this.TimerBanner_Tick);
            // 
            // FoodLogo
            // 
            this.FoodLogo.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("FoodLogo.BackgroundImage")));
            this.FoodLogo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.FoodLogo.ErrorImage = null;
            this.FoodLogo.InitialImage = ((System.Drawing.Image)(resources.GetObject("FoodLogo.InitialImage")));
            this.FoodLogo.Location = new System.Drawing.Point(29, 39);
            this.FoodLogo.Name = "FoodLogo";
            this.FoodLogo.Size = new System.Drawing.Size(236, 50);
            this.FoodLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.FoodLogo.TabIndex = 25;
            this.FoodLogo.TabStop = false;
            // 
            // CustomerDisplayPOS
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ClientSize = new System.Drawing.Size(1364, 766);
            this.ControlBox = false;
            this.Controls.Add(this.Frame_Superior);
            this.Controls.Add(this.Frame_Abajo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CustomerDisplayPOS";
            this.ShowInTaskbar = false;
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Frame_Abajo.ResumeLayout(false);
            this.Frame_Abajo.PerformLayout();
            this.Panel2.ResumeLayout(false);
            this.Panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Fact)).EndInit();
            this.Panel1.ResumeLayout(false);
            this.Panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LogoBz)).EndInit();
            this.Frame_Superior.ResumeLayout(false);
            this.Frame_Superior.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Surt_plus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Precio_plus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Limp_plus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Aten_plus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.msgGracias)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Aten)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Surt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Precio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Limp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicOK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCancelPlus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicOKPlus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FoodLogo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Timer TimerUpdateDatas;
        private System.Windows.Forms.Panel Frame_Abajo;
        internal System.Windows.Forms.Panel Panel2;
        internal System.Windows.Forms.Panel Panel1;
        internal System.Windows.Forms.Label Impuesto;
        internal System.Windows.Forms.Label Peso;
        internal System.Windows.Forms.Label Subtotal;
        internal System.Windows.Forms.Label Lbl_Impuesto;
        internal System.Windows.Forms.Label Lbl_Peso;
        internal System.Windows.Forms.Label Lbl_Subtotal;
        internal System.Windows.Forms.Label Lbl_Cedula;
        internal System.Windows.Forms.Label Lbl_Nombre;
        private System.Windows.Forms.Panel Frame_Superior;
        private System.Windows.Forms.PictureBox PicOKPlus;
        private System.Windows.Forms.PictureBox PicCancelPlus;
        private System.Windows.Forms.PictureBox LogoBz;
        private System.Windows.Forms.PictureBox PicCancel;
        private System.Windows.Forms.PictureBox PicOK;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label MsgCaras;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox Surt;
        private System.Windows.Forms.PictureBox Precio;
        private System.Windows.Forms.PictureBox Limp;
        internal System.Windows.Forms.DataGridView Grd_Fact;
        private System.Windows.Forms.PictureBox Aten;
        private System.Windows.Forms.Timer TimerBanner;
        private System.Windows.Forms.PictureBox msgGracias;
        private System.Windows.Forms.PictureBox Surt_plus;
        private System.Windows.Forms.PictureBox Precio_plus;
        private System.Windows.Forms.PictureBox Limp_plus;
        private System.Windows.Forms.PictureBox Aten_plus;
        private System.Windows.Forms.Panel Panel3;
        internal System.Windows.Forms.Label Total;
        internal System.Windows.Forms.Label Lbl_Total;
        internal System.Windows.Forms.Label Descuento;
        internal System.Windows.Forms.Label lbl_Descuento;
        internal System.Windows.Forms.Label Donacion;
        internal System.Windows.Forms.Label lbl_Donacion;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblGranTotal;
        internal System.Windows.Forms.Label lblTotAlt2;
        internal System.Windows.Forms.Label lblTotAlt1;
        private System.Windows.Forms.TextBox txtTestFontSize1;
        private System.Windows.Forms.TextBox txtTestLen1;
        private System.Windows.Forms.Label lblGranTotalAlt2;
        private System.Windows.Forms.Label lblGranTotalAlt1;
        internal System.Windows.Forms.Label MontoIGTF;
        internal System.Windows.Forms.Label lbl_IGTF;
        private System.Windows.Forms.PictureBox FoodLogo;
    }
}

