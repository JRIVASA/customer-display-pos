﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data;
using System.Data.SqlClient;
using System.Runtime.InteropServices;
using System.Diagnostics;
using CustomerDisplayPOS.Class;
using System.IO;
using System.Reflection;
using System.Security.Policy;

namespace CustomerDisplayPOS
{

    public partial class CustomerDisplayPOS : Form
    {

        public Int32 nRegistros = 0;

        public const Int32 WM_NCLBUTTONDOWN = 0xA1;
        public const Int32 HT_CAPTION = 0x2;
        private const Int32 SW_RESTORE = 9;
        
        private SizeF currentScaleFactor = new SizeF(1f, 1f);
        [DllImportAttribute("user32.dll")]
        public static extern Int32 SendMessage(IntPtr hWnd, Int32 Msg, Int32 wParam, Int32 lParam);
        [DllImportAttribute("user32.dll")]
        public static extern Boolean ReleaseCapture();

        [DllImport("user32.dll")]
        private static extern Boolean
        SetForegroundWindow(IntPtr hWnd);

        [DllImport("user32.dll")]
        private static extern Boolean ShowWindowAsync(
        IntPtr hWnd, Int32 nCmdShow);

        public Int32 ContadorCaiman = 1;
        public Int32 ContadorCaiman2 = 0;

        public dynamic ObjBalanza = null;

        public static String gPath = Assembly.GetExecutingAssembly().Location.Replace(Assembly.GetExecutingAssembly().GetName().Name + ".exe", String.Empty);
        
        public Size CurrentScreenSize = new Size();

        public static FSLogger Logger;

        private Boolean FlagTmp1 = false;

        Size Panel1_Size = new Size();
        Point LogoBz_Location = new Point();
        Size LogoBz_Size = new Size();

        Point Lbl_Subtotal_Location = new Point();
        Point lbl_Descuento_Location = new Point();
        Point Lbl_Impuesto_Location = new Point();
        Point lbl_Donacion_Location = new Point();
        Point Lbl_IGTF_Location = new Point();
        Point Subtotal_Location = new Point();
        Point Impuesto_Location = new Point();
        Point Descuento_Location = new Point();
        Point Donacion_Location = new Point();
        Point MontoIGTF_Location = new Point();

        Point Panel3_Location = new Point();
        Size Panel3_Size = new Size();
        Point Total_Location = new Point();
        float Total_Font_Size = 0;
        Point lblTotAlt1_Location = new Point();
        Point lblTotAlt2_Location = new Point();

        Point Panel2_Location = new Point();
        Size Panel2_Size = new Size();
        Point Lbl_Peso_Location = new Point();
        Size Lbl_Peso_Size = new Size();
        Point Peso_Location = new Point();
        Size Peso_Size = new Size();

        private Boolean SoportaIGTF = false;
        private Boolean PrimeraVezIGTF = true;

        Point PreIGTF_Lbl_Subtotal_Location = new Point();
        Point PreIGTF_lbl_Descuento_Location = new Point();
        Point PreIGTF_Lbl_Impuesto_Location = new Point();
        Point PreIGTF_lbl_Donacion_Location = new Point();
        Point PreIGTF_Subtotal_Location = new Point();
        Point PreIGTF_Impuesto_Location = new Point();
        Point PreIGTF_Descuento_Location = new Point();
        Point PreIGTF_Donacion_Location = new Point();

        public CustomerDisplayPOS()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

            //PruebaLecturaBalanza();

            try
            {

                Logger = new FSLogger(gPath + "Log.txt");

                Subtotal.Text = "0";
                Descuento.Text = "0";
                Impuesto.Text = "0";
                Total.Text = "0";
                Peso.Text = "0";

                Lbl_Nombre.Text = String.Empty;
                Lbl_Cedula.Text = String.Empty;

                Grd_Fact.RowTemplate.Height = 40;

                TimerUpdateDatas.Interval = Properties.Settings.Default.MainRefreshTimeInMilliseconds;

                //FillData();            

                this.Frame_Superior.BackgroundImage = null;

                LoadImagenBg();

                SetFormLocation();

                AjustarForma();

                ChangeIMG();

                //label1.Font = new Font("Open Sans", 20, FontStyle.Bold);
                //label2.Font = new Font("Open Sans", 20, FontStyle.Bold);
                //label3.Font = new Font("Open Sans", 12, FontStyle.Regular);
                //label4.Font = new Font("Open Sans", 12, FontStyle.Regular);

                //this.Lbl_Subtotal.Font = new Font("Open Sans", 14, FontStyle.Regular);
                //this.lbl_Descuento.Font = new Font("Open Sans", 14, FontStyle.Regular);
                //this.Lbl_Impuesto.Font = new Font("Open Sans", 14, FontStyle.Regular);
                //this.Lbl_Peso.Font = new Font("Open Sans", 14, FontStyle.Regular);
                //this.Lbl_Total.Font = new Font("Open Sans", 16, FontStyle.Bold);

                if (Properties.Settings.Default.Balanza_EmularDisplayPeso)
                {

                    if (!String.IsNullOrEmpty(Properties.Settings.Default.WeightLabel))
                    {
                        this.Lbl_Peso.Text = Properties.Settings.Default.WeightLabel;
                    }

                    if (String.IsNullOrEmpty(Properties.Settings.Default.WeightLabel) && Properties.Settings.Default.ConvertirKilosEnLibras)
                    {
                        this.Lbl_Peso.Text = "Weight (Lb)";
                    }

                    else if (String.IsNullOrEmpty(Properties.Settings.Default.WeightLabel) && !Properties.Settings.Default.ConvertirKilosEnLibras)
                    {
                        this.Lbl_Peso.Text = "Weight (Kg)";
                    }

                }
                else
                {

                    if (Properties.Settings.Default.MonedaAdicional1AMostrar.isUndefined())
                    {
                        Panel3.Height = Panel3.Height + Panel2.Height + (Panel2.Top - (Panel3.Top + Panel3.Height));
                        Panel2.Visible = false;
                        Lbl_Total.Top += Convert.ToInt32(Lbl_Total.Top + (Panel2.Height / 8));
                        Total.Top += Convert.ToInt32(Total.Top + (Panel2.Height / 12));
                    }

                }

                Lbl_Subtotal.Text = Properties.Settings.Default.SubtotalCaption + ":";
                lbl_Descuento.Text = Properties.Settings.Default.DescuentoCaption + ":";

                if (String.IsNullOrEmpty(Properties.Settings.Default.TaxID))
                {
                    Lbl_Impuesto.Visible = false;
                    Impuesto.Visible = false;
                }
                else
                {
                    Lbl_Impuesto.Text = Properties.Settings.Default.TaxID + ":";
                }

                if (!String.IsNullOrEmpty(Properties.Settings.Default.TotalCaption))
                {
                    Lbl_Total.Text = Properties.Settings.Default.TotalCaption + ":";
                }
                else
                {
                    //Lbl_Total.Visible = false;
                    //Total.Font = new Font(Total.Font.FontFamily.Name, 34);
                    //Total.ForeColor = Color.Firebrick; // ColorTranslator.FromHtml("#121BB5");
                    ////Total.AutoSize = true;
                    //Total.Top = Lbl_Total.Top;
                    //Total.Left = Total.Left - 1;
                    //Total.Height += Lbl_Total.Height;
                    //Panel3.BackColor = Panel2.BackColor;
                    //Total.TextAlign = ContentAlignment.MiddleCenter;
                }

                if (!String.IsNullOrEmpty(Properties.Settings.Default.DonacionCaption))
                {
                    lbl_Donacion.Text = Properties.Settings.Default.DonacionCaption + ":";
                    lbl_Donacion.Visible = true;
                    Donacion.Visible = true;
                }
                else
                {
                    lbl_Donacion.Visible = false;
                }

                if (Properties.Settings.Default.GranTotalEnabled)
                {

                    lblGranTotal.AutoSize = false;
                    lblGranTotalAlt1.AutoSize = false;
                    lblGranTotalAlt2.AutoSize = false;

                    if (Properties.Settings.Default.MonedaAdicional1AMostrar.isUndefined())
                        lblGranTotal.Size = Frame_Superior.Size;
                    else
                    {
                        lblGranTotal.Size = new Size(Frame_Superior.Width, Frame_Superior.Height / 2);
                        if (!Properties.Settings.Default.MonedaAdicional2AMostrar.isUndefined())
                        {
                            lblGranTotalAlt1.Size = new Size(Frame_Superior.Width / 2, Frame_Superior.Height / 2);
                            lblGranTotalAlt2.Size = new Size(Frame_Superior.Width / 2, Frame_Superior.Height / 2);
                        }
                        else
                            lblGranTotalAlt1.Size = new Size(Frame_Superior.Width, Frame_Superior.Height / 2);
                    }
                    
                    if (Properties.Settings.Default.GranTotalFont == null)
                        lblGranTotal.Font = new Font(lblGranTotal.Font.FontFamily, 72);
                    else
                        lblGranTotal.Font = Properties.Settings.Default.GranTotalFont;

                    //lblGranTotalAlt1.Font = new Font(lblGranTotal.Font.Name, lblGranTotal.Font.Size / 2, lblGranTotal.Font.Style, lblGranTotal.Font.Unit);
                    lblGranTotalAlt1.Font = lblGranTotal.Font;
                    lblGranTotalAlt2.Font = lblGranTotal.Font;
					
                    if (!Properties.Settings.Default.ModoFood)
                    {
                        lblGranTotal.ForeColor = Properties.Settings.Default.GranTotalColor;
                        FoodLogo.Enabled = false;
                        FoodLogo.Visible = false;
                        LogoBz.Visible = true;
                        LogoBz.Enabled = true;
                    }
                    else {
                        lblGranTotal.ForeColor = Color.FromArgb(1, 244, 126, 32);
                        LogoBz.Visible = false;
                        LogoBz.Enabled = false;
                        FoodLogo.Visible = true;
                        Grd_Fact.DefaultCellStyle.SelectionBackColor = Color.FromArgb(244, 126, 32);
                    }
					
                    if (Properties.Settings.Default.MonedaAdicional1AMostrar.isUndefined())
                        lblGranTotal.TextAlign = ContentAlignment.MiddleCenter;
                    else
                        lblGranTotal.TextAlign = ContentAlignment.BottomCenter;

                    lblGranTotal.Visible = true;
                    lblGranTotal.Top = 0;
                    lblGranTotal.Left = 0;

                    lblGranTotalAlt1.ForeColor = Color.FromArgb(0, 128, 0);
                    lblGranTotalAlt2.ForeColor = Color.FromArgb(0, 0, 192);

                    lblGranTotalAlt1.TextAlign = ContentAlignment.TopCenter;
                    lblGranTotalAlt2.TextAlign = ContentAlignment.TopCenter;

                    lblGranTotal.BringToFront();

                }

                //this.Lbl_Peso.Text = Properties.Settings.Default.WeightLabel;

                if (String.IsNullOrEmpty(Properties.Settings.Default.ImgRoot) && !String.IsNullOrEmpty(Properties.Settings.Default.ImgName))
                { 
                    this.TimerBanner.Enabled = false;
                }

                FillData();

            }
            catch(Exception ex)
            {
                Logger.EscribirLog(ex, "Load: ");
            }

            //this.Total.Font = new Font("Open Sans", 20, FontStyle.Bold);

        }
        private void Form1_MouseDown(object sender,System.Windows.Forms.MouseEventArgs e)
        {
            try 
            {
                if (e.Button == MouseButtons.Left)
                {
                    ReleaseCapture();
                    SendMessage(this.Handle, 0x112, 0xf012, 0);
                }
            }
            catch(Exception Any)
            {
                Console.WriteLine(Any.Message);
            }
        }
        
        void FillData()
        {

            Double SubtotalMain;
            Double TaxMain;
            Double TotalMain;

            if (Properties.Settings.Default.FileMode)
            {
                
                List<Product> Productos = new List<Product>();

                try
                {

                    String[] Codigos = new String[] { "000001", "000002", "000003" };
                    String[] Nombres = new String[] { "Impresora HP M102W LaserJet Pro Monocromática", "Multifuncional Brother 800W Ink Tank Tinta Color DCP-T800W", "Bocinas Logitech Z313 2.1 Alambrica Negra" };
                    Double[] Precios = new Double[] { 127.11, 232.00, 312.30 };
                    Double[] Impuesto = new Double[] { 10, 20, 30 };
                    Double[] Cantidades = new Double[] { 1, 2, 1 };
                    Double[] Totales = new Double[] { 110.10, 440.40, 330.30 };

                    Double PrecioTotal = 0;
                    Double ImpuestoTotal = 0;
                    Double TotalTotal = 0;

                    if (ContadorCaiman == 1)
                    { 
                        ReiniciarControles(); // cuando llega al inicio reinicio todo
                    }
                    
                    for (Int32 i = 0; i < ContadorCaiman; i++)
                    {
                        Product Producto = new Product();

                        //Producto.Code = Codigos[i];
                        Producto.Descripcion = Nombres[i];
                        Producto.Precio = (Precios[i]).ToString("#.##");
                        Producto.Cantidad = Cantidades[i];
                        Producto.Impuesto = (Precios[i] * 0.10).ToString("#.##");
                        Producto.Total = ((Precios[i] * 1.10) * Cantidades[i]).ToString("#.##");

                        PrecioTotal = PrecioTotal + Precios[i];
                        ImpuestoTotal = ImpuestoTotal + (Precios[i] * 0.10);
                        TotalTotal = TotalTotal + ((Precios[i] * 1.10) * Cantidades[i]);

                        PrecioTotal = Math.Round(PrecioTotal,2);
                        ImpuestoTotal = Math.Round(ImpuestoTotal,2);
                        TotalTotal = Math.Round(TotalTotal,2);

                        this.Subtotal.Text = PrecioTotal.ToString("##,###,##0.00");
                        this.Impuesto.Text = ImpuestoTotal.ToString("##,###,##0.00");
                        this.Peso.Text = "0.00";
                        this.Total.Text = TotalTotal.ToString("##,###,##0.00");

                        //this.Subtotal.Text = this.Subtotal.Text.ToString().Format("{0:N}");//string.Format('{0:n}', this.Subtotal);

                        Productos.Add(Producto);

                    }

                    var bindingList = new BindingList<Product>(Productos);
                    var source = new BindingSource(bindingList, null);
                    Grd_Fact.DataSource = source;

                    if (ContadorCaiman == 3)
                    {
                        ContadorCaiman = 1;
                    }
                    else
                    {
                        ContadorCaiman++;
                    }

                    if (this.Grd_Fact.Rows.Count > 0)
                    {
                        Grd_Fact.Rows[Grd_Fact.Rows.Count - 1].Selected = true;
                        Grd_Fact.CurrentCell = Grd_Fact.Rows[Grd_Fact.Rows.Count - 1].Cells[1];
                    }
                    
                }
                catch (Exception ex)
                {
                    Logger.EscribirLog(ex, "FillData: ");
                }

            }
            else//si no es por archivo
            { 

                try
                {

                    //AjustarGrid();

                    Functions f = new Functions();

                    String SQLQuery = "SELECT Descripcion AS DESCRIPCION, Cantidad AS CANTIDAD, " +
                    "ROUND(Precio, 2, 0) AS PRECIO, ROUND(Impuesto, 2, 0) AS IMPUESTO, ROUND(Total, 2, 0) AS TOTAL " +
                    "FROM TR_TRANSACCION_TEMP " +
                    "ORDER BY ID ";
                    
                    SQLQuery = "SELECT Descripcion AS DESCRIPTION, Cantidad AS ITEMS, ROUND(Precio, 2, 0) AS PRICE, " +
                    "ROUND(Impuesto, 2, 0) AS TAX, ROUND(Total, 2, 0) AS TOTAL " +
                    "FROM TR_TRANSACCION_TEMP " +
                    "ORDER BY ID ";

                    String SQLQueryFood = "SELECT TR.cs_Descripcion_Producto AS PRODUCT, " +
                    "FORMAT(TR.nu_Cantidad,'N' + CAST(PR.Cant_Decimales AS nvarchar(MAX)), 'en-us') AS ITEMS, " +
                    "FORMAT(ROUND(TR.ns_Precio , 2, 0), 'N', 'en-us') AS PRICE, " +
                    "FORMAT(ROUND(TR.ns_Impuesto, 2, 0), 'N', 'en-us') AS TAX, " +
                    "FORMAT(ROUND(TR.ns_Total, 2, 0), 'N', 'en-us') AS TOTAL " +
                    "FROM TR_TRANSACCION_TEMP TR, MA_TRANSACCION_TEMP MA, MA_PRODUCTOS PR " +
                    "WHERE TR.ns_Codigo_Principal = PR.c_Codigo " +
                    "AND TR.ns_IDHijo = MA.cs_IDPadre " +
                    "AND MA.bs_Activa = 1 " +
                    "AND TR.cs_Codigo_Producto_Padre = '' ";

                    // 2008 R2 AND LOWER
                    //SQLQuery = "SELECT TR.Descripcion AS PRODUCT, CASE WHEN ROUND(TR.Cantidad - CAST(TR.Cantidad AS NUMERIC(18, 0)), 8, 0) <> 0 THEN SUBSTRING(CAST(CAST(TR.Cantidad AS NUMERIC(28, 10)) AS NVARCHAR(MAX)), 1, ((1 + LEN(CAST(CAST(TR.Cantidad AS NUMERIC(18,0)) AS NVARCHAR(MAX))) + PR.Cant_Decimales))) ELSE CAST(CAST(TR.Cantidad AS NUMERIC(18, 0)) AS NVARCHAR(MAX)) END AS ITEMS, CONVERT(NVARCHAR(MAX), CAST(TR.Precio AS MONEY), 1) AS PRICE, CONVERT(NVARCHAR(MAX), CAST(TR.Impuesto AS MONEY), 1) AS TAX, CONVERT(NVARCHAR(MAX), CAST(TR.Total AS MONEY), 1) AS TOTAL FROM TR_TRANSACCION_TEMP TR INNER JOIN MA_PRODUCTOS PR ON PR.c_Codigo = TR.Cod_Principal INNER JOIN MA_TRANSACCION_TEMP MA ON MA.ID_Padre = TR.ID_Hijo WHERE MA.Activa = 1 ORDER BY TR.ID";
                    // 2012 AND HIGHER
                    //SQLQuery = "SELECT TR.Descripcion AS PRODUCT, FORMAT(TR.Cantidad, 'N' + CAST(PR.Cant_Decimales AS NVARCHAR(MAX)), 'en-us') AS ITEMS, FORMAT(ROUND(TR.Precio, 2, 0), 'N', 'en-us') AS PRICE, FORMAT(ROUND(TR.Impuesto, 2, 0), 'N', 'en-us') AS TAX, FORMAT(ROUND(TR.Total, 2, 0), 'N', 'en-us') AS TOTAL FROM TR_TRANSACCION_TEMP TR INNER JOIN MA_PRODUCTOS PR ON PR.c_Codigo = TR.Cod_Principal INNER JOIN MA_TRANSACCION_TEMP MA ON MA.ID_Padre = TR.ID_Hijo WHERE MA.Activa = 1 ORDER BY TR.ID";

                    if (!String.IsNullOrEmpty(Properties.Settings.Default.Query))
                    {
                        if (Properties.Settings.Default.ModoFood)
                        {
                            if (!String.IsNullOrEmpty(Properties.Settings.Default.QueryFOOD))
                            {
                                SQLQuery = Properties.Settings.Default.QueryFOOD;
                            }
                            else {
                                SQLQuery = SQLQueryFood;
                            }
                        }
                        else
                        {
                            SQLQuery = Properties.Settings.Default.Query;
                        }
                    }

                    SqlDataAdapter DatAdap = new SqlDataAdapter(SQLQuery, f.connectionString);

                    DataSet ds = new DataSet();

                    f.connectionString.Open();

                    DatAdap.Fill(ds, "TR_TRANSACCION_TEMP");

                    nRegistros = ds.Tables.Cast<DataTable>().Sum(X => X.Rows.Count); 

                    /*if (nRegistros == Grd_Fact.RowCount)
                    {

                        if (nRegistros == 0)
                        {

                            this.PicOK.Visible = true;
                            this.PicOKPlus.Visible = false;
                            this.PicCancel.Visible = true;
                            this.PicCancelPlus.Visible = false;
                            //this.FondoNerson.Enabled = false;
                            this.PicOK.Enabled = false;
                            this.PicCancel.Enabled = false;
                            this.MsgCaras.Visible = false;
                            this.Subtotal.Text = "0";
                            this.Impuesto.Text = "0";
                            this.Peso.Text = "0";
                            this.Donacion.Text = "0";
                            this.Total.Text = "0";

                            pictureBox2.Visible = true;
                            pictureBox5.Visible = false;
   
                            pictureBox3.Visible = true;
                            Limp.Visible = false;

                            pictureBox4.Visible = true;
                            Precio.Visible = false;
  
                            pictureBox1.Visible = true;
                            Surt.Visible = false;
                        }

                        goto CloseCon;

                    }*/
                    // Por que estaba ese codigo ahi? No dejaba refrescar el grid cuando solamente cambiaba el contenido de las filas, mas no la cantidad...

                    this.PicOK.Enabled = true;
                    this.PicCancel.Enabled = true;

                    Grd_Fact.DataSource = ds;
                    Grd_Fact.DataMember = "TR_TRANSACCION_TEMP";
                    //Grd_Fact.Refresh();

                    if (this.Grd_Fact.Rows.Count > 0)
                    {
                        AjustarGrid();
                        Grd_Fact.Rows[Grd_Fact.Rows.Count - 1].Selected = true;
                        Grd_Fact.CurrentCell = Grd_Fact.Rows[Grd_Fact.Rows.Count - 1].Cells[1];
                    }

                    //Functions f = new Functions();

                CloseCon:

                    SqlCommand cm = null;
                    SqlDataReader rs = null;

                    Boolean AplicaDcto = false;
                    Boolean MostrarIGTF = false;
                    Double Subt = 0, Impu = 0, Tot = 0, Dcto = 0, SubtNeto = 0, MontoDonacion = 0, ImpIGTF = 0, WG = 0;
                    Double TotFmt = 0, TotAlt1 = 0, TotAlt2 = 0;
                    String mFmtDecMonPred, mFmtMonPred;
                    String mFmtDecMonAlt1, mFmtMonAlt1;
                    String mFmtDecMonAlt2, mFmtMonAlt2;

                    if (Functions.ExisteTabla("TMP_MA_TRANSACCION_DESCUENTOS", f.connectionString))
                    {

                        SQLQuery = "SELECT Top 1 * FROM TMP_MA_TRANSACCION_DESCUENTOS";

                        cm = new SqlCommand(SQLQuery, f.connectionString);

                        rs = cm.ExecuteReader();

                        if (rs.Read())
                        {

                            SubtNeto = Convert.ToDouble(rs["n_Subtotal"]);
                            Impu = Convert.ToDouble(rs["n_Impuesto"]);
                            Tot = Convert.ToDouble(rs["n_Total"]);
                            Dcto = Convert.ToDouble(rs["n_Descuento"]);
                            Subt = SubtNeto + Dcto;
                            WG = 0;

                            this.Subtotal.Text = Subt.ToString();
                            this.Descuento.Text = Dcto.ToString();
                            this.Impuesto.Text = Impu.ToString();

                            if (ExisteCampoRs(rs, "n_Donacion"))
                                MontoDonacion = Convert.ToDouble(rs["n_Donacion"]);
                            else
                                MontoDonacion = 0;

                            if (ExisteCampoRs(rs, "n_Monto_IGTF"))
                                ImpIGTF = Convert.ToDouble(rs["n_Monto_IGTF"]);
                            else
                                ImpIGTF = 0;

                            MostrarIGTF = (ImpIGTF != 0);

                            if (!SoportaIGTF) // Si alguna compra tiene IGTF, entonces soporta IGTF
                                if (MostrarIGTF)
                                    SoportaIGTF = true;

                            this.Donacion.Text = MontoDonacion.ToString();
                            this.Total.Text = Tot.ToString();
                            this.Peso.Text = WG.ToString();
                            this.MontoIGTF.Text = ImpIGTF.ToString();

                            lbl_Descuento.Visible = true;
                            Descuento.Visible = true;
                            AplicaDcto = true;

                        }

                        rs.Close();

                    }

                    // Obtener Datos de Moneda Predeterminada.

                    DataSet mRsMonPred = new DataSet();
                    DataSet mRsMonAdic1 = null;
                    DataSet mRsMonAdic2 = null;
                    String QueryMoneda;
                    if (Properties.Settings.Default.ModoFood)
                    {
                        QueryMoneda = "SELECT TOP 1 * FROM " +
                        "ADM_LOCAL_FOOD.DBO.MA_MONEDAS WHERE b_Preferencia = 1";

                    }
                    else
                    {
                        QueryMoneda = "SELECT TOP 1 * FROM " +
                        "ADM_LOCAL.DBO.MA_MONEDAS WHERE b_Preferencia = 1";
                    }
                    
                    SqlCommand mSqlCmd = new SqlCommand(QueryMoneda, f.connectionString);
                    SqlDataAdapter TmpAdapter = new SqlDataAdapter(mSqlCmd);
                    TmpAdapter.Fill(mRsMonPred, "RS");

                    // USE: mRsMonPred.Tables["RS"].Rows[0].ItemArray

                    if (!Properties.Settings.Default.MonedaAdicional1AMostrar.isUndefined())
                    {

                        mRsMonAdic1 = new DataSet();

                        if (Properties.Settings.Default.ModoFood)
                        {
                            mSqlCmd = Functions.getParameterizedCommand("SELECT * FROM " +
                                "ADM_LOCAL_FOOD.DBO.MA_MONEDAS " +
                                "WHERE c_CodMoneda = @Moneda", f.connectionString, new SqlParameter[]{
                            new SqlParameter(){ ParameterName = "@Moneda", Value = Properties.Settings.Default.MonedaAdicional1AMostrar }
                            });
                        }
                        else
                        {
                            mSqlCmd = Functions.getParameterizedCommand("SELECT * FROM " +
                                "ADM_LOCAL.DBO.MA_MONEDAS " +
                                "WHERE c_CodMoneda = @Moneda", f.connectionString, new SqlParameter[]{
                            new SqlParameter(){ ParameterName = "@Moneda", Value = Properties.Settings.Default.MonedaAdicional1AMostrar }
                            });
                        }

                        TmpAdapter = new SqlDataAdapter(mSqlCmd);
                        TmpAdapter.Fill(mRsMonAdic1, "RS");

                        if (mRsMonAdic1.Tables["RS"].Rows.Count <= 0)
                        {
                            mRsMonAdic1 = null;
                        }

                    }

                    if (!Properties.Settings.Default.MonedaAdicional2AMostrar.isUndefined() 
                    && !Properties.Settings.Default.MonedaAdicional1AMostrar.isUndefined())
                    {

                        mRsMonAdic2 = new DataSet();

                        if (Properties.Settings.Default.ModoFood)
                        {
                            mSqlCmd = Functions.getParameterizedCommand("SELECT * FROM " +
                                "ADM_LOCAL_FOOD.DBO.MA_MONEDAS " +
                                "WHERE c_CodMoneda = @Moneda", f.connectionString, new SqlParameter[]{
                            new SqlParameter(){ ParameterName = "@Moneda", Value = Properties.Settings.Default.MonedaAdicional2AMostrar }
                            });
                        }
                        else
                        {
                            mSqlCmd = Functions.getParameterizedCommand("SELECT * FROM " +
                                "ADM_LOCAL.DBO.MA_MONEDAS " +
                                "WHERE c_CodMoneda = @Moneda", f.connectionString, new SqlParameter[]{
                            new SqlParameter(){ ParameterName = "@Moneda", Value = Properties.Settings.Default.MonedaAdicional2AMostrar }
                            });
                        }

                        TmpAdapter = new SqlDataAdapter(mSqlCmd);
                        TmpAdapter.Fill(mRsMonAdic2, "RS");

                        if (mRsMonAdic2.Tables["RS"].Rows.Count <= 0)
                        {
                            mRsMonAdic2 = null;
                        }

                    }

                    Int32 DecMonPred = 2, DecMonAlt1 = 2, DecMonAlt2 = 2;

                    Program.CodMonPred = mRsMonPred.Tables["RS"].Rows[0].Field<String>("c_CodMoneda");
                    Program.DesMonPred = mRsMonPred.Tables["RS"].Rows[0].Field<String>("c_Descripcion");
                    Program.FacMonPred = mRsMonPred.Tables["RS"].Rows[0].Field<Double>("n_Factor");
                    Program.DecMonPred = mRsMonPred.Tables["RS"].Rows[0].Field<Int32>("n_Decimales");
                    Program.SimMonPred = mRsMonPred.Tables["RS"].Rows[0].Field<String>("c_Simbolo");
                    
                    DecMonPred = Program.DecMonPred;

                    mFmtDecMonPred = (DecMonPred == 0 ? String.Empty : "." + String.Empty.PadRight(DecMonPred, '0'));
                    mFmtMonPred = "#,###,###,###,##0" + mFmtDecMonPred;

                    if (!(mRsMonAdic1 is null))
                    {

                        AjustarFrameTotalesMultimoneda();

                        if (Properties.Settings.Default.MonedaAdicional1AMostrar_Decimales >= 0)
                        {
                            DecMonAlt1 = Properties.Settings.Default.MonedaAdicional1AMostrar_Decimales;
                        } else { DecMonAlt1 = mRsMonAdic1.Tables["RS"].Rows[0].Field<Int32>("n_Decimales"); }

                    }

                    if (!(mRsMonAdic2 is null))
                    {

                        if (Properties.Settings.Default.MonedaAdicional2AMostrar_Decimales >= 0)
                        {
                            DecMonAlt2 = Properties.Settings.Default.MonedaAdicional2AMostrar_Decimales;
                        }
                        else { DecMonAlt2 = mRsMonAdic2.Tables["RS"].Rows[0].Field<Int32>("n_Decimales"); }

                    }

                    String CampoActiva;
                    
                    CampoActiva = Properties.Settings.Default.ModoFood ? "bs_Activa" : "Activa";
                    
                    if (Properties.Settings.Default.ModoFood)
                    {
                        SQLQuery = "SELECT * FROM MA_DISPLAY_TEMP WHERE " + CampoActiva + " = 1";
                    }
                    else
                    {
                        SQLQuery = "SELECT * FROM MA_TRANSACCION_TEMP WHERE " + CampoActiva + " = 1";
                    }
					
                    cm = new SqlCommand(SQLQuery, f.connectionString);

                    rs = cm.ExecuteReader();

                    if (rs.Read())
                    {

                        if (!Properties.Settings.Default.ModoFood)
                        {

                            if (!AplicaDcto)
                            {
                                Dcto = 0;
                                Subt = Convert.ToDouble(rs["Subtotal"]);
                                SubtNeto = Subt;

                                Impu = Convert.ToDouble(rs["Impuesto"]);
                                Tot = Convert.ToDouble(rs["Total"]);

                                this.Subtotal.Text = Subt.ToString();
                                this.Impuesto.Text = Impu.ToString();
                                this.Total.Text = Tot.ToString();
                            }

                            this.Peso.Text = WG.ToString();
                            this.Donacion.Text = Donacion.ToString();

                            this.Lbl_Nombre.Text = Convert.ToString(rs["Nombre"]);
                            this.Lbl_Cedula.Text = Convert.ToString(rs["Codigo"]);

                        }
                        else
                        {

                            if (!AplicaDcto)
                            {
                                Dcto = 0;
                                Subt = Convert.ToDouble(rs["nu_Monto_Subtotal"]);
                                SubtNeto = Subt;

                                Impu = Convert.ToDouble(rs["nu_Monto_Impuesto"]);
                                Tot = Convert.ToDouble(rs["nu_Monto_Total"]);

                                this.Subtotal.Text = Subt.ToString();
                                this.Impuesto.Text = Impu.ToString();
                                this.Total.Text = Tot.ToString();
                            }

                            this.Peso.Text = WG.ToString();
                            this.Donacion.Text = Donacion.ToString();

                            this.Lbl_Nombre.Text = Convert.ToString(rs["cu_Nombre_Cliente"]);
                            this.Lbl_Cedula.Text = Convert.ToString(rs["cu_Codigo_Cliente"]);

                        }

                    }

                    /*if (!AplicaDcto)
                    {
                        lbl_Descuento.Visible = false;
                        Descuento.Visible = false;
                        Descuento.Text = Dcto.ToString();
                    }*/
                    
                    // Mejor Mostrar siempre el descuento asi sea cero. Ese espacio en blanco entre
                    // subtotal e impuesto no da buena visual a la interfaz

                    if (!lbl_Descuento.Text.Equals(String.Empty))
                    {
                        lbl_Descuento.Visible = true;
                        Descuento.Visible = true;
                    }

                    if (SoportaIGTF && PrimeraVezIGTF)
                    {
                        
                        PrimeraVezIGTF = false;

                        PreIGTF_Lbl_Subtotal_Location = Lbl_Subtotal.Location;
                        PreIGTF_lbl_Descuento_Location = lbl_Descuento.Location;
                        PreIGTF_Lbl_Impuesto_Location = Lbl_Impuesto.Location;
                        PreIGTF_lbl_Donacion_Location = lbl_Donacion.Location;

                        PreIGTF_Subtotal_Location = Subtotal.Location;
                        PreIGTF_Descuento_Location = Descuento.Location;
                        PreIGTF_Impuesto_Location = Impuesto.Location;
                        PreIGTF_Donacion_Location = Donacion.Location;

                    }

                    if (SoportaIGTF)
                    {

                        if (MostrarIGTF)
                        {
                            if (Donacion.Visible)
                            {

                                Int32 mTopIGTFMove = 0;

                                if (Math.Abs(CurrentScreenSize.Width - 1920) <= 10 && Math.Abs(CurrentScreenSize.Height - 1080) <= 10)
                                {

                                    mTopIGTFMove = 25;

                                    Lbl_Subtotal.Location = new Point(PreIGTF_Lbl_Subtotal_Location.X, PreIGTF_Lbl_Subtotal_Location.Y - mTopIGTFMove);
                                    lbl_Descuento.Location = new Point(PreIGTF_lbl_Descuento_Location.X, PreIGTF_lbl_Descuento_Location.Y - mTopIGTFMove);
                                    Lbl_Impuesto.Location = new Point(PreIGTF_Lbl_Impuesto_Location.X, PreIGTF_Lbl_Impuesto_Location.Y - mTopIGTFMove);
                                    lbl_Donacion.Location = new Point(PreIGTF_lbl_Donacion_Location.X, PreIGTF_lbl_Donacion_Location.Y - mTopIGTFMove);
                                    lbl_IGTF.Location = new Point(PreIGTF_lbl_Donacion_Location.X, PreIGTF_lbl_Donacion_Location.Y + 12);

                                    Subtotal.Location = new Point(PreIGTF_Subtotal_Location.X, PreIGTF_Subtotal_Location.Y - mTopIGTFMove);
                                    Descuento.Location = new Point(PreIGTF_Descuento_Location.X, PreIGTF_Descuento_Location.Y - mTopIGTFMove);
                                    Impuesto.Location = new Point(PreIGTF_Impuesto_Location.X, PreIGTF_Impuesto_Location.Y - mTopIGTFMove);
                                    Donacion.Location = new Point(PreIGTF_Donacion_Location.X, PreIGTF_Donacion_Location.Y - mTopIGTFMove);
                                    MontoIGTF.Location = new Point(PreIGTF_Donacion_Location.X, PreIGTF_Donacion_Location.Y + 9);

                                }
                                else if (Math.Abs(CurrentScreenSize.Width - 1366) <= 10 && Math.Abs(CurrentScreenSize.Height - 768) <= 10)
                                {

                                    mTopIGTFMove = 18;

                                    Lbl_Subtotal.Location = new Point(PreIGTF_Lbl_Subtotal_Location.X, PreIGTF_Lbl_Subtotal_Location.Y - mTopIGTFMove);
                                    lbl_Descuento.Location = new Point(PreIGTF_lbl_Descuento_Location.X, PreIGTF_lbl_Descuento_Location.Y - mTopIGTFMove);
                                    Lbl_Impuesto.Location = new Point(PreIGTF_Lbl_Impuesto_Location.X, PreIGTF_Lbl_Impuesto_Location.Y - mTopIGTFMove);
                                    lbl_Donacion.Location = new Point(PreIGTF_lbl_Donacion_Location.X, PreIGTF_lbl_Donacion_Location.Y - mTopIGTFMove);
                                    lbl_IGTF.Location = new Point(PreIGTF_lbl_Donacion_Location.X, PreIGTF_lbl_Donacion_Location.Y + 8);

                                    Subtotal.Location = new Point(PreIGTF_Subtotal_Location.X, PreIGTF_Subtotal_Location.Y - mTopIGTFMove);
                                    Descuento.Location = new Point(PreIGTF_Descuento_Location.X, PreIGTF_Descuento_Location.Y - mTopIGTFMove);
                                    Impuesto.Location = new Point(PreIGTF_Impuesto_Location.X, PreIGTF_Impuesto_Location.Y - mTopIGTFMove);
                                    Donacion.Location = new Point(PreIGTF_Donacion_Location.X, PreIGTF_Donacion_Location.Y - mTopIGTFMove);
                                    MontoIGTF.Location = new Point(PreIGTF_Donacion_Location.X, PreIGTF_Donacion_Location.Y + 9);

                                }
                                else if (Math.Abs(CurrentScreenSize.Width - 1024) <= 10 && Math.Abs(CurrentScreenSize.Height - 768) <= 10)
                                {

                                    mTopIGTFMove = 15;

                                    Lbl_Subtotal.Location = new Point(PreIGTF_Lbl_Subtotal_Location.X, PreIGTF_Lbl_Subtotal_Location.Y - mTopIGTFMove);
                                    lbl_Descuento.Location = new Point(PreIGTF_lbl_Descuento_Location.X, PreIGTF_lbl_Descuento_Location.Y - mTopIGTFMove);
                                    Lbl_Impuesto.Location = new Point(PreIGTF_Lbl_Impuesto_Location.X, PreIGTF_Lbl_Impuesto_Location.Y - mTopIGTFMove);
                                    lbl_Donacion.Location = new Point(PreIGTF_lbl_Donacion_Location.X, PreIGTF_lbl_Donacion_Location.Y - mTopIGTFMove);
                                    lbl_IGTF.Location = new Point(PreIGTF_lbl_Donacion_Location.X, PreIGTF_lbl_Donacion_Location.Y + 14);

                                    Subtotal.Location = new Point(PreIGTF_Subtotal_Location.X, PreIGTF_Subtotal_Location.Y - mTopIGTFMove);
                                    Descuento.Location = new Point(PreIGTF_Descuento_Location.X, PreIGTF_Descuento_Location.Y - mTopIGTFMove);
                                    Impuesto.Location = new Point(PreIGTF_Impuesto_Location.X, PreIGTF_Impuesto_Location.Y - mTopIGTFMove);
                                    Donacion.Location = new Point(PreIGTF_Donacion_Location.X, PreIGTF_Donacion_Location.Y - mTopIGTFMove);
                                    MontoIGTF.Location = new Point(PreIGTF_Donacion_Location.X, PreIGTF_Donacion_Location.Y + 12);

                                }
                                else if (Math.Abs(CurrentScreenSize.Width - 800) <= 10 && Math.Abs(CurrentScreenSize.Height - 600) <= 10)
                                {

                                    mTopIGTFMove = 12;

                                    Lbl_Subtotal.Location = new Point(PreIGTF_Lbl_Subtotal_Location.X, PreIGTF_Lbl_Subtotal_Location.Y - mTopIGTFMove);
                                    lbl_Descuento.Location = new Point(PreIGTF_lbl_Descuento_Location.X, PreIGTF_lbl_Descuento_Location.Y - mTopIGTFMove);
                                    Lbl_Impuesto.Location = new Point(PreIGTF_Lbl_Impuesto_Location.X, PreIGTF_Lbl_Impuesto_Location.Y - mTopIGTFMove);
                                    lbl_Donacion.Location = new Point(PreIGTF_lbl_Donacion_Location.X, PreIGTF_lbl_Donacion_Location.Y - mTopIGTFMove);
                                    lbl_IGTF.Location = new Point(PreIGTF_lbl_Donacion_Location.X, PreIGTF_lbl_Donacion_Location.Y + 11);

                                    Subtotal.Location = new Point(PreIGTF_Subtotal_Location.X, PreIGTF_Subtotal_Location.Y - mTopIGTFMove);
                                    Descuento.Location = new Point(PreIGTF_Descuento_Location.X, PreIGTF_Descuento_Location.Y - mTopIGTFMove);
                                    Impuesto.Location = new Point(PreIGTF_Impuesto_Location.X, PreIGTF_Impuesto_Location.Y - mTopIGTFMove);
                                    Donacion.Location = new Point(PreIGTF_Donacion_Location.X, PreIGTF_Donacion_Location.Y - mTopIGTFMove);
                                    MontoIGTF.Location = new Point(PreIGTF_Donacion_Location.X, PreIGTF_Donacion_Location.Y + 8);

                                }

                            } else
                            {
                                lbl_IGTF.Location = lbl_Donacion.Location;
                                MontoIGTF.Location = Donacion.Location;
                            }

                            lbl_IGTF.Visible = true;
                            MontoIGTF.Visible = true;

                        }
                        else
                        {

                             Lbl_Subtotal.Location = PreIGTF_Lbl_Subtotal_Location;
                             lbl_Descuento.Location = PreIGTF_lbl_Descuento_Location;
                             Lbl_Impuesto.Location = PreIGTF_Lbl_Impuesto_Location;
                             lbl_Donacion.Location = PreIGTF_lbl_Donacion_Location;

                             Subtotal.Location = PreIGTF_Subtotal_Location;
                             Descuento.Location = PreIGTF_Descuento_Location;
                             Impuesto.Location = PreIGTF_Impuesto_Location;
                             Donacion.Location = PreIGTF_Donacion_Location;

                            lbl_IGTF.Visible = false;
                            MontoIGTF.Visible = false;

                        }

                    }
                    
                    TotFmt = Convert.ToDouble(Tot.ToString(mFmtMonPred, System.Globalization.CultureInfo.InvariantCulture));

                    if (!(mRsMonAdic1 is null))
                    {

                        TotAlt1 = Functions.MontoExacto(mRsMonAdic1.Tables["RS"].Rows[0].Field<String>("c_CodMoneda"),
                        TotFmt, mRsMonAdic1.Tables["RS"].Rows[0].Field<Double>("n_Factor"), DecMonAlt1);

                        mFmtDecMonAlt1 = (DecMonAlt1 == 0 ? String.Empty : "." + String.Empty.PadRight(DecMonAlt1, '0'));
                        mFmtMonAlt1 = "#,###,###,###,##0" + mFmtDecMonAlt1;

                        lblTotAlt1.Text = mRsMonAdic1.Tables["RS"].Rows[0].Field<String>("c_Simbolo") + " " + TotAlt1.ToString(mFmtMonAlt1);

                        lblGranTotalAlt1.Text = lblTotAlt1.Text;

                        lblTotAlt1.Visible = true;

                    }

                    if (!(mRsMonAdic2 is null))
                    {

                        TotAlt2 = Functions.MontoExacto(mRsMonAdic2.Tables["RS"].Rows[0].Field<String>("c_CodMoneda"),
                        TotFmt, mRsMonAdic2.Tables["RS"].Rows[0].Field<Double>("n_Factor"), DecMonAlt2);

                        mFmtDecMonAlt2 = (DecMonAlt2 == 0 ? String.Empty : "." + String.Empty.PadRight(DecMonAlt2, '0'));
                        mFmtMonAlt2 = "#,###,###,###,##0" + mFmtDecMonAlt2;

                        lblTotAlt2.Text = mRsMonAdic2.Tables["RS"].Rows[0].Field<String>("c_Simbolo") + " " + TotAlt2.ToString(mFmtMonAlt2);

                        lblGranTotalAlt2.Text = lblTotAlt2.Text;

                        lblTotAlt2.Visible = true;

                    }

                    //this.Subtotal.Text = PrecioTotal.ToString("##,###,##0.00");

                    /*this.Subtotal.Text = String.Format("##,###,##0.00", this.Subtotal.Text);
                    this.Impuesto.Text = String.Format("##,###,##0.00", this.Impuesto.Text);
                    this.Peso.Text = String.Format("##,###,##0.00", this.Peso.Text);
                    this.Total.Text = String.Format("##,###,##0.00", this.Total.Text);
                    */

                    //string SubTotalLb = this.Subtotal.Text;
                    //string ImpuestoLb = this.Impuesto.Text;
                    //string TotalLb = this.Total.Text;

                    //TotalLb = String.Format("{0,0.0}", TotalLb);
                    //SubTotalLb=String.Format("{0:#,###,###.00}", SubTotalLb);
                    //ImpuestoLb = String.Format("N1",ImpuestoLb);
                    //TotalLb = String.Format(System.Globalization.CultureInfo.InvariantCulture, "{0:0,0.0}", TotalLb);
                    //String.Format(CultureInfo.InvariantCulture,"{0:0,0.0}", value)
                    //this.Subtotal.Text = CustomNumberFormat(SubTotalLb.ToString());
                    //this.Impuesto.Text = CustomNumberFormat(ImpuestoLb.ToString());
                    //this.Total.Text = CustomNumberFormat(TotalLb.ToString());

                    //this.Subtotal.Text = this.Subtotal.Text.ToString().Format("{0:N}");//string.Format('{0:n}', this.Subtotal);
                    //this.Subtotal.Text.Format("{0:N}");

                    //this.Subtotal.Text = String.Format("{0:N}", Convert.ToDouble(this.Subtotal.Text)); //Convert.ToString(Math.Round(double.Parse(this.Subtotal.Text),2));
                    //this.Impuesto.Text = String.Format("{0:N}", Convert.ToDouble(this.Impuesto.Text)); //Convert.ToString(Math.Round(double.Parse(this.Impuesto.Text ),2));
                    //this.Descuento.Text = String.Format("{0:N}", Convert.ToDouble(this.Descuento.Text)); //Convert.ToString(Math.Round(double.Parse(this.Impuesto.Text ),2));
                    //this.Donacion.Text = String.Format("{0:N}", Convert.ToDouble(this.Donacion.Text)); //Convert.ToString(Math.Round(double.Parse(this.Donacion.Text ),2));
                    //this.Peso.Text = String.Format("{0:N}", Convert.ToDouble(this.Peso.Text)); //Convert.ToString(Math.Round(double.Parse(this.Peso.Text),2));
                    //this.Total.Text = String.Format("{0:N}", Convert.ToDouble(this.Total.Text)); //Convert.ToString(Math.Round(double.Parse(this.Total.Text), 2));

                    this.Subtotal.Text = Subt.ToString(mFmtMonPred);
                    this.Impuesto.Text = Impu.ToString(mFmtMonPred);
                    this.Descuento.Text = Dcto.ToString(mFmtMonPred);
                    this.Donacion.Text = MontoDonacion.ToString(mFmtMonPred);
                    this.MontoIGTF.Text = ImpIGTF.ToString(mFmtMonPred);
                    this.Peso.Text = WG.ToString(mFmtMonPred);
                    this.Total.Text = Program.SimMonPred + " " + Tot.ToString(mFmtMonPred); // + " " + Program.SimMonPred;

                    this.lblGranTotal.Text = this.Total.Text; //Tot.ToString(mFmtMonPred);

                    f.connectionString.Close();

                }
                catch (SqlException Any)
                {
                    
                    if (!(Any.Number == 208 && Any.Message.Contains("TMP_PRODUCTOS_EN_PROMOCION", StringComparison.OrdinalIgnoreCase)))
                    {
                        Logger.EscribirLog(Any, "FillData: ");
                    } else
                    {
                        Console.WriteLine(Any.Message);
                        // Ignore.
                    }

                }
                catch (Exception Any)
                {
                    Console.WriteLine(Any.Message);
                    Logger.EscribirLog(Any, "FillData: ");
                }

            }//end else filemode

        }

        void SetFormLocation()
        {
            try
            {

                Screen[] Pantallas = Screen.AllScreens;

                Int32 SetupScreen = (Properties.Settings.Default.NumScreen - 1);

                Int32 TmpValidNumScreen = ((SetupScreen > (Pantallas.Length - 1)) ?
                0 : SetupScreen);

                Point location = Pantallas[TmpValidNumScreen].Bounds.Location;
                Size size = Pantallas[TmpValidNumScreen].Bounds.Size;

                this.Left = location.X;
                this.Top = location.Y;
                this.WindowState = FormWindowState.Maximized;
                this.Width = size.Width;
                this.Height = size.Height;

            }
            catch (Exception Any)
            {
                Console.WriteLine(Any.Message);
                //Logger.EscribirLog(ex, "SetFormLocation():");
            }
        }

        private void AjustarGrid()
        {
            try 
            {
                if (this.Grd_Fact.RowCount > 0)
                {   

                    Grd_Fact.EnableHeadersVisualStyles = false;
                    Grd_Fact.ColumnHeadersDefaultCellStyle.BackColor = Color.FromArgb(64, 64, 64);
                    Grd_Fact.BorderStyle = BorderStyle.None;
                    Grd_Fact.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.None;
                    Grd_Fact.Font = new Font("Verdana", Convert.ToSingle(Properties.Settings.Default.FontSize), GraphicsUnit.Pixel);
                    Grd_Fact.Font = new Font("Open Sans", Convert.ToSingle(Properties.Settings.Default.FontSize), GraphicsUnit.Pixel);

                    Grd_Fact.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;

                    /*DataGridViewColumn column = Grd_Fact.Columns[0];//decs
                    
                    //column.Width = (Convert.ToInt32(this.Grd_Fact.Width * 0.40));
                    column.FillWeight = 300;
                    //column.InheritedAutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode;
                    //column.InheritedAutoSizeMode = true;
                    DataGridViewColumn column2 = Grd_Fact.Columns[1];//cant
                    column2.FillWeight = 100;
                    //column2.Width = Convert.ToInt32(this.Grd_Fact.Width * 0.15);
                    DataGridViewColumn column3 = Grd_Fact.Columns[2];//precio
                    column3.FillWeight = 100;
                    //column3.Width = Convert.ToInt32(this.Grd_Fact.Width * 0.15);
                    DataGridViewColumn column4 = Grd_Fact.Columns[3];//imp
                    column4.FillWeight = 100;
                    //column4.Width = Convert.ToInt32(this.Grd_Fact.Width * 0.15);
                    DataGridViewColumn column5 = Grd_Fact.Columns[4];//total
                    column5.FillWeight = 100;
                    //column4.Width = Convert.ToInt32(this.Grd_Fact.Width * 0.15);
                     * */

                    //this.Grd_Fact.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

                    //string ASD=Properties.Settings.Default.ColumnWeight;
                    String[] Weights = Properties.Settings.Default.ColumnWeight.Split(';');
                    Int32 W = 0;

                    foreach (var Weight in Weights)
                    {

                        //System.Console.WriteLine($"<{word}>");
                        DataGridViewColumn column = Grd_Fact.Columns[W];//cant
                        column.FillWeight = float.Parse(Weight);
                        W++;

                    }

                    String[] Alignments = Properties.Settings.Default.ColumnAlignment.Split(';');
                    Int32 A = 0;

                    foreach (var Align in Alignments)
                    {

                        //System.Console.WriteLine($"<{word}>");

                        switch(Align)
                        {
                            case "L":
                                this.Grd_Fact.Columns[A].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
                                this.Grd_Fact.Columns[A].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
                                break;
                            case "M":
                                this.Grd_Fact.Columns[A].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                                this.Grd_Fact.Columns[A].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                                break;
                            case "R":
                                this.Grd_Fact.Columns[A].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                                this.Grd_Fact.Columns[A].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                                break;
                        }

                        A++;

                    }

                    //this.Grd_Fact.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                    //this.Grd_Fact.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                    //this.Grd_Fact.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                    //this.Grd_Fact.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                    
                    /*foreach (DataGridViewColumn col in Grd_Fact.Columns)
                    {
                        //col.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                    }*/

                }

            }
            catch(Exception ex)
            {
                Logger.EscribirLog(ex, "AjustarGrid(): ");
            }

        }

        private void AjustarForma()
        {
            try
            {

                Screen[] Pantallas = Screen.AllScreens;

                Int32 SetupScreen = (Properties.Settings.Default.NumScreen - 1);

                Int32 TmpValidNumScreen = ((SetupScreen > (Pantallas.Length - 1)) ?
                0 : SetupScreen);

                Point TmpLocation = Pantallas[TmpValidNumScreen].Bounds.Location;
                CurrentScreenSize = Pantallas[TmpValidNumScreen].Bounds.Size;

                float scaleX = (float)this.Size.Width / 1366;
                float scaley = (float)this.Size.Height / 768;
                //this.currentScaleFactor = new SizeF(this.currentScaleFactor.Width * scaleX, this.currentScaleFactor.Height * scaley);
                this.currentScaleFactor = new SizeF(scaleX, scaley);
                //ajusta todo el panel de la parte de abajo
                this.Frame_Abajo.Scale(this.currentScaleFactor);
                this.Frame_Superior.Scale(this.currentScaleFactor);
                //this.Frame_Grid.Scale(this.currentScaleFactor);

                // Nuevo 2021-06-12. A partir de ahora vamos a soportar unas resoluciones especificas para este programa.
                // Y vamos a asegurarnos de que el programa se vea bien en esas resoluciones haciendo a pie las modificaciones
                // que haya que hacer. (Esto va a implicar varios Ifs por resolucion). Inicialmente vamos a asegurar que el 
                // programa se vea bien en las siguientes resoluciones: 800x600, 1024x768, 1366x768 y 1920x1080.
                // A nivel de programa este form esta siendo diseñado y trabajado en resolución 1366x768, por lo cual primero, 
                // aseguraremos que se vea bien en esta. Luego haremos los Ifs necesarios para adaptar al resto de resoluciones.

                if (Math.Abs(CurrentScreenSize.Width - 1920) <= 10 && Math.Abs(CurrentScreenSize.Height - 1080) <= 10)
                {
                    // 1920x1080
                    // Ajustar 1
                    Lbl_Total.Font = new Font(Lbl_Total.Font.Name, 32F, Lbl_Total.Font.Style, Lbl_Total.Font.Unit);
                    Peso.Font = new Font(Peso.Font.Name, 26F, Peso.Font.Style, Peso.Font.Unit);
                    //LogoBz.Location = new Point(91, -30);
                } else if (Math.Abs(CurrentScreenSize.Width - 1366) <= 10 && Math.Abs(CurrentScreenSize.Height - 768) <= 10)
                {
                    // 1366x768
                    // Ajustar 2
                    Lbl_Total.Font = new Font(Lbl_Total.Font.Name, 22F, Lbl_Total.Font.Style, Lbl_Total.Font.Unit);
                    Lbl_Subtotal.Font = new Font(Lbl_Subtotal.Font.Name, 17F, Lbl_Subtotal.Font.Style, Lbl_Subtotal.Font.Unit);
                    Lbl_Impuesto.Font = new Font(Lbl_Impuesto.Font.Name, 17F, Lbl_Impuesto.Font.Style, Lbl_Impuesto.Font.Unit);
                    lbl_Descuento.Font = new Font(lbl_Descuento.Font.Name, 17F, lbl_Descuento.Font.Style, lbl_Descuento.Font.Unit);
                    lbl_Donacion.Font = new Font(lbl_Donacion.Font.Name, 17F, lbl_Donacion.Font.Style, lbl_Donacion.Font.Unit);
                    lbl_IGTF.Font = new Font(lbl_IGTF.Font.Name, 17F, lbl_IGTF.Font.Style, lbl_IGTF.Font.Unit);
                }
                else if (Math.Abs(CurrentScreenSize.Width - 1024) <= 10 && Math.Abs(CurrentScreenSize.Height - 768) <= 10)
                {
                    // 1024x768
                    // Ajustar 3
                    Lbl_Total.Font = new Font(Lbl_Total.Font.Name, 20F, Lbl_Total.Font.Style, Lbl_Total.Font.Unit);
                    Lbl_Subtotal.Font = new Font(Lbl_Subtotal.Font.Name, 13F, Lbl_Subtotal.Font.Style, Lbl_Subtotal.Font.Unit);
                    Lbl_Impuesto.Font = new Font(Lbl_Impuesto.Font.Name, 13F, Lbl_Impuesto.Font.Style, Lbl_Impuesto.Font.Unit);
                    lbl_Descuento.Font = new Font(lbl_Descuento.Font.Name, 13F, lbl_Descuento.Font.Style, lbl_Descuento.Font.Unit);
                    lbl_Donacion.Font = new Font(lbl_Donacion.Font.Name, 13F, lbl_Donacion.Font.Style, lbl_Donacion.Font.Unit);
                    lbl_IGTF.Font = new Font(lbl_IGTF.Font.Name, 13F, lbl_IGTF.Font.Style, lbl_IGTF.Font.Unit);
                }
                else if (Math.Abs(CurrentScreenSize.Width - 800) <= 10 && Math.Abs(CurrentScreenSize.Height - 600) <= 10)
                {
                    // 800x600
                    // Ajustar 4
                    Lbl_Total.Font = new Font(Lbl_Total.Font.Name, 15F, Lbl_Total.Font.Style, Lbl_Total.Font.Unit);
                    Lbl_Subtotal.Font = new Font(Lbl_Subtotal.Font.Name, 10F, Lbl_Subtotal.Font.Style, Lbl_Subtotal.Font.Unit);
                    Lbl_Impuesto.Font = new Font(Lbl_Impuesto.Font.Name, 10F, Lbl_Impuesto.Font.Style, Lbl_Impuesto.Font.Unit);
                    lbl_Descuento.Font = new Font(lbl_Descuento.Font.Name, 10F, lbl_Descuento.Font.Style, lbl_Descuento.Font.Unit);
                    lbl_Donacion.Font = new Font(lbl_Donacion.Font.Name, 10F, lbl_Donacion.Font.Style, lbl_Donacion.Font.Unit);
                    lbl_IGTF.Font = new Font(lbl_IGTF.Font.Name, 10F, lbl_IGTF.Font.Style, lbl_IGTF.Font.Unit);
                }

            }
            catch (Exception ex)
            {
                Logger.EscribirLog(ex, "AjustarForma(): ");
            }
        }

        private void TimerUpdateDates_Tick(object sender, EventArgs e)
        {

            FillData();

            if (Properties.Settings.Default.Balanza_EmularDisplayPeso)
            {
                ObjBalanza = null;
                CreateObjBalanza();
                UpdateLecturaBalanza();
            }

        }

        private void ChangeIMG()
        {

            try
            { 
                String MyPath = Application.StartupPath;// +"\\" + Properties.Settings.Default.ImgName;
                String imgRoot = Properties.Settings.Default.ImgRoot;

                List<String> ext = new List<String> { "jpg", "png" };

                //this.Frame_Superior.BackgroundImage = Image.FromFile(ImgRoot);
                //System.Collections.Generic.List<FileInfo> FilesInfo = Directory.GetFiles(MyPath, "*.Jpg*");
                
                String[] filePaths = Directory.GetFiles(Application.StartupPath, "*.png");
                
                if (Properties.Settings.Default.ImgRoot == "")
                {
                    filePaths = Directory.GetFiles(Application.StartupPath, "*.png");
                }
                else
                {
                    filePaths = Directory.GetFiles(Application.StartupPath + "\\" + Properties.Settings.Default.ImgRoot, "*.png");
                }
                
                //string[] filePaths = Directory.GetFiles(Application.StartupPath + "" + Properties.Settings.Default.ImgRoot, "*.*", SearchOption.AllDirectories);
                //string[] filePaths = Application.StartupPath.(path, "*.jpg");
                //var files = Directory.GetFiles(MyPath, "*.*", SearchOption.AllDirectories)
                //.Where(s => ext.Contains(Path.GetExtension(s)));
                
                ContadorCaiman2++;
                
                if (ContadorCaiman2 == filePaths.Count())
                {
                    ContadorCaiman2 = 0;
                }
                
                this.Frame_Superior.BackgroundImage = Image.FromFile(filePaths[ContadorCaiman2]);

                if (Properties.Settings.Default.GranTotalEnabled)
                {

                    this.lblGranTotal.BringToFront();
                    
                    if (this.lblGranTotalAlt1.Visible)
                        this.lblGranTotalAlt1.BringToFront();
                    
                    if (this.lblGranTotalAlt2.Visible)
                        this.lblGranTotalAlt2.BringToFront();

                }
                    

            }
            catch(Exception ex)
            {
                Logger.EscribirLog(ex, "ChangeIMG(): ");
            }
            
        }

        private void PicOK_Click(object sender, EventArgs e)
        {
            if (PicCancelPlus.Visible == false)
            {
                this.PicOK.Visible = false;
                this.PicCancel.Visible = false;
                //this.PicOKPlus.Visible = true;
                //this.MsgCaras.Visible = true;
                this.msgGracias.Visible = true;
            }
        }

        private void PicCancel_Click(object sender, EventArgs e)
        {   
            if (PicOKPlus.Visible == false)
            {
                this.PicCancel.Visible = false;
                this.PicOK.Visible = false;
                //this.PicCancelPlus.Visible = true;
                //this.MsgCaras.Visible = true;
                this.msgGracias.Visible = true;
            }
        }

        private void LogoBz_Click(object sender, EventArgs e)
        {
            if(Properties.Settings.Default.ExitOption)
            { 
                this.Close();
            }
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            pictureBox2.Visible = false;
            pictureBox5.Visible = true;
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            pictureBox3.Visible = false;
            Limp.Visible = true;
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            pictureBox4.Visible = false;
            Precio.Visible = true;
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            pictureBox1.Visible = false;
            Surt.Visible = true;
        }

        private void LoadImagenBg()
        {
            try
            { 
                String ImgRoot = Application.StartupPath + "\\" + Properties.Settings.Default.ImgName;
                this.Frame_Superior.BackgroundImage = Image.FromFile(ImgRoot);
            }
            catch (Exception Any)
            {
                Console.WriteLine(Any.Message);
                //lo dejo asi para que en caso de que de error simplementen no carge la img
            }
        }
        private void LoadImagenBg(int Numero)
        {
            try
            {
                String ImgRoot = Application.StartupPath + "\\" + Properties.Settings.Default.ImgName;
                this.Frame_Superior.BackgroundImage = Image.FromFile(ImgRoot);
            }
            catch
            {
                //lo dejo asi para que en caso de que de error simplementen no carge la img
            }
        }

        private void PicCancelPlus_Click(object sender, EventArgs e)
        {
            //this.MsgCaras.Visible = true;
            this.msgGracias.Visible = true;
            //this.PicOKPlus.Visible = false;
            this.PicCancelPlus.Visible = true;
        }

        private void PicOKPlus_Click(object sender, EventArgs e)
        {
            //this.MsgCaras.Visible = true;
            this.msgGracias.Visible = true;
            //this.PicOKPlus.Visible = false;
            this.PicCancelPlus.Visible = true;
        }

        private void TimerBanner_Tick(object sender, EventArgs e)
        {
            ChangeIMG();
        }

        private void Aten_Click(object sender, EventArgs e)
        {
            this.Aten_plus.Visible = true;
        }

        private void Limp_Click(object sender, EventArgs e)
        {
            this.Limp_plus.Visible = true;
        }

        private void Precio_Click(object sender, EventArgs e)
        {
            this.Precio_plus.Visible = true;
        }

        private void Surt_Click(object sender, EventArgs e)
        {
            this.Surt_plus.Visible = true;
        }
        private void ReiniciarControles()
        {
            try
            { 
                this.Surt_plus.Visible = false;
                this.Aten_plus.Visible = false;
                this.Limp_plus.Visible = false;
                this.Precio_plus.Visible = false;
                this.msgGracias.Visible = false;

                this.PicOK.Visible = true;
                this.PicCancel.Visible = true;

                this.Subtotal.Text = "0";
                this.Impuesto.Text = "0";
                this.Peso.Text = "0";
                this.Total.Text = "0";
            }
            catch(Exception Any)
            {
                Console.WriteLine(Any.Message);
            }
        }

        private void Total_Click(object sender, EventArgs e)
        {

        }

        private Boolean ExisteCampoRs(SqlDataReader pRs, String pCampo)
        {
            try
            {
                Object X = pRs["n_Donacion"];
                return true;
            }
            catch (Exception Any)
            {
                Console.WriteLine(Any.Message);
                return false;
            }
            
        }
        private void Frame_Abajo_Paint(object sender, PaintEventArgs e)
        {

        }

        private void CreateObjBalanza()
        {
            try 
            {
                ObjBalanza = Functions.SafeCreateObject("StellarBalanzaDisplay.Handler");
            }
            catch(Exception ex)
            {
                Logger.EscribirLog(ex, "CreateObjBalanza(): ");
            }
        }

        private void UpdateLecturaBalanza()
        {

            try 
            {

                if(ObjBalanza != null)
                {
                    if(Properties.Settings.Default.ConvertirKilosEnLibras)
                    {
                        Double Peso=0;
                        //this.Total.Text = TotalTotal.ToString("#.##");
                        //Peso = Double.Parse(ObjBalanza.UltimaLectura) * 2.204622;
                        Peso = (CalibrarPesoKilosALibrasPrecisionCeroPuntoCeroCeroCincoGramos(ObjBalanza.UltimaLectura));
                        //this.Peso.Text = (Peso) * 2.204622;
                        this.Peso.Text = Peso.ToString("0.00");
                    }
                    else
                    {
                        Double Peso = 0;
                        //this.Total.Text = TotalTotal.ToString("#.##");
                        Peso = Double.Parse(ObjBalanza.UltimaLectura);
                        //Peso = Double.Parse("325.2466");
                        this.Peso.Text = Peso.ToString("0.00");
                    }
                }
                
            }
            catch(COMException ex)
            {
                if(ex.ErrorCode == -2147023174)
                {
                    //error de cierre del componente COM
                }
                else
                {
                    Logger.EscribirLog(ex, "COMException UpdateLecturaBalanza():");
                }
            }
            catch(Exception ex)
            {
                Logger.EscribirLog(ex, "UpdateLecturaBalanza():");
                ObjBalanza = null;
            }

        }

        private void Peso_Click(object sender, EventArgs e)
        {

        }

        public double CalibrarPesoKilosALibrasPrecisionCeroPuntoCeroCeroCincoGramos(String pLecturaOrigen)
        {

            //por la precicion de la balanza hay una factor de perdida
            //y no se pueden mostrar los valores exactos por lo cual con esta 
            //funcion aproximamos los valores a la parte entera tomado en cuenta
            //unos valores promedio leidos manualmente, de los cuales se saca el valor del mas o menos 0.0003
            // 17-09 -2018 Caso Meat Club

            try
            {

                Double TmpNum;
                Double TmpFixUnd;
        
                //const Double MargenErrorBalanza = 0.005 ;//' Datalogic Magellan 9300i
                const Double FactorPerdidaPrecisionPromedioPorLibra = 0.003;
                const Double CuantasLBHayEnUnKG = 2.20462262185;
        
                TmpNum = (Convert.ToDouble(pLecturaOrigen) * CuantasLBHayEnUnKG);
                TmpNum = ((Math.Truncate((TmpNum * 100)) / 100) + ((Math.Truncate((TmpNum * 10)) / 10) * FactorPerdidaPrecisionPromedioPorLibra));
                TmpNum = Math.Round(TmpNum, 2);
        
                TmpFixUnd = Math.Round(TmpNum, 0);
        
                switch(RoundFixValue(TmpFixUnd))
                {
                    case 1:
                        if(Math.Round(Math.Abs(TmpNum - TmpFixUnd), 2) <= 0.01)
                        {
                            TmpNum = TmpFixUnd;
                        }
                        break;
                    case 2:
                        if(Math.Round((TmpNum - TmpFixUnd), 2) >= 0.01 && Math.Round((TmpNum - TmpFixUnd), 2) <= 0.02 )
                        {
                                TmpNum = TmpFixUnd;
                        }
                        break;
                    case 3:
                        if(Math.Round((TmpNum - TmpFixUnd), 2) >= 0.01 && Math.Round((TmpNum - TmpFixUnd), 2) <= 0.03 )
                        {
                                TmpNum = TmpFixUnd;
                        }
                        break;
                    case 4:
                        if(Math.Round((TmpNum - TmpFixUnd), 2) >= 0.01 && Math.Round((TmpNum - TmpFixUnd), 2) <= 0.04 )
                        {
                                TmpNum = TmpFixUnd;
                        }
                        break;
                    case 5:
                        if(Math.Round((TmpNum - TmpFixUnd), 2) >= 0.01 && Math.Round((TmpNum - TmpFixUnd), 2) <= 0.05 )
                        {
                                TmpNum = TmpFixUnd;
                        }
                        break;
                    case 6:
                        if(Math.Round((TmpNum - TmpFixUnd), 2) >= 0.01 && Math.Round((TmpNum - TmpFixUnd), 2) <= 0.06 )
                        {
                                TmpNum = TmpFixUnd;
                        }
                        break;
                }
                return TmpNum;
            }
            catch(Exception ex)
            {
                //Logger.EscribirLog(ex, "CalibrarPesoKilosALibrasPrecisionCeroPuntoCeroCeroCincoGramos():");
                return 0;
            }
            
        }
        
        private int RoundFixValue(Double Value)
        {
            try
            {
                if(Value >= 0 && Value <= 7)
                {
                    return 1;
                }
                if(Value >= 8 && Value <= 16)
                {
                    return 2;
                }
                if(Value >= 17 && Value <= 25)
                {
                    return 3;
                }
                if(Value >= 26 && Value <= 28)
                {
                    return 4;
                }
                if(Value >= 29 && Value <= 30)
                {
                    return 5;
                }
                else
                {
                    return 6;
                }
            }
            catch(Exception ex)
            {
                //Logger.EscribirLog(ex, "RoundFixValue():");
                return 1;
            }       
        }

        public String CustomNumberFormat(String myNumber)
        {

            String Number = "0";
            
            try
            { 
                
                Int32 dot = myNumber.IndexOf(".");
                Number="0";

                Double Numberd = Double.Parse(myNumber)*100;
                Numberd = Math.Round(Numberd, 2);

                Int32 dot2 = Numberd.ToString().IndexOf(".");
                //Numberd = Numberd / 100;

                String strNumberWithoutDecimals = myNumber.Substring(0, (dot == -1 ? myNumber.Length : dot));
                String strNumberDecimals = (dot == -1 ? "" : myNumber.Substring(dot));

                strNumberWithoutDecimals = Numberd.ToString().Substring(0, (dot2 == -1 ? Numberd.ToString().Length : dot2));
                strNumberDecimals = (dot2 == -1 ? "00" : myNumber.Substring(dot));

                //strNumberWithoutDecimals = System.Convert.ToInt32(strNumberWithoutDecimals).ToString("#,##0");

                strNumberWithoutDecimals = (dot == -1 ? myNumber : Numberd.ToString().Substring(0, dot));
                strNumberWithoutDecimals = System.Convert.ToInt32(strNumberWithoutDecimals).ToString("##,###,##0");
                strNumberDecimals = (dot == -1 ? "00" : Numberd.ToString().Substring(dot));

                Number = strNumberWithoutDecimals + "." +strNumberDecimals;

                return Number;

                //var s = string.Format("{0:0,0.00}", Number);

                /*if (s.EndsWith("00"))
                {
                    //return ((int)Number).ToString();
                    return (s);
                }
                else
                {
                    return s;
                }*/

            }
            catch (Exception ex)
            {
                //Logger.EscribirLog(ex, "CustomNumberFormat():");
                return Number;
            }    

        }

        private void Grd_Fact_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void Lbl_Total_Click(object sender, EventArgs e)
        {

        }

        private Int32 PointFactor(Int32 A, Double B)
        {
            return (Int32)(((Double)A) * B);
        }

        private void AjustarFrameTotalesMultimoneda()
        {

            if (!FlagTmp1)
            {

                FlagTmp1 = true;
                
                Panel1_Size = Panel1.Size;
                LogoBz_Location = LogoBz.Location;
                LogoBz_Size = LogoBz.Size;

                FoodLogo.Location = LogoBz_Location;
                FoodLogo.Size = LogoBz_Size;
                
                Lbl_Subtotal_Location = Lbl_Subtotal.Location;
                lbl_Descuento_Location = lbl_Descuento.Location;
                Lbl_Impuesto_Location = Lbl_Impuesto.Location;
                lbl_Donacion_Location = lbl_Donacion.Location;
                Lbl_IGTF_Location = lbl_IGTF.Location;
                Subtotal_Location = Subtotal.Location;
                Impuesto_Location = Impuesto.Location;
                Descuento_Location = Descuento.Location;
                Donacion_Location = Donacion.Location;
                MontoIGTF_Location = MontoIGTF.Location;

                Panel3_Location = Panel3.Location;
                Panel3_Size = Panel3.Size;
                Total_Location = Total.Location;
                Total_Font_Size = Total.Font.Size;

                Panel2_Location = Panel2.Location;
                Panel2_Size = Panel2.Size;
                Lbl_Peso_Location = Lbl_Peso.Location;
                Lbl_Peso_Size = Lbl_Peso.Size;
                Peso_Location = Peso.Location;
                Peso_Size = Peso.Size;

                if (Properties.Settings.Default.GranTotalEnabled)
                    if (!Properties.Settings.Default.MonedaAdicional2AMostrar.isUndefined())
                    {
                        lblGranTotalAlt2.Left = 0;
                        lblGranTotalAlt2.Top = (Int32)((Frame_Superior.Height / 2) * 1D);
                        lblGranTotalAlt2.Height = Frame_Superior.Height / 2;
                        lblGranTotalAlt2.Width = Frame_Superior.Width / 2;
                        lblGranTotalAlt2.Visible = true;
                        lblGranTotalAlt2.BringToFront();

                        lblGranTotalAlt1.Left = Frame_Superior.Width / 2;
                        lblGranTotalAlt1.Top = (Int32)((Frame_Superior.Height / 2) * 1D);
                        lblGranTotalAlt1.Height = Frame_Superior.Height / 2;
                        lblGranTotalAlt1.Width = Frame_Superior.Width / 2;
                        lblGranTotalAlt1.Visible = true;
                        lblGranTotalAlt1.BringToFront();

                    } else
                    {
                        lblGranTotalAlt1.Left = 0;
                        lblGranTotalAlt1.Top = (Int32)((Frame_Superior.Height / 2) * 1D);
                        lblGranTotalAlt1.Height = Frame_Superior.Height / 2;
                        lblGranTotalAlt1.Width = Frame_Superior.Width;
                        lblGranTotalAlt1.Visible = true;
                        lblGranTotalAlt1.BringToFront();
                    }

            }

            if (Math.Abs(CurrentScreenSize.Width - 1920) <= 10 && Math.Abs(CurrentScreenSize.Height - 1080) <= 10)
            {

                Panel1.Size = new Size(Panel1_Size.Width, PointFactor(Panel1_Size.Height, 0.915));
                //LogoBz.Location = new Point(LogoBz_Location.X, PointFactor(LogoBz_Location.Y, -0.55));
                FoodLogo.BringToFront();
                Lbl_Subtotal.Location = new Point(Lbl_Subtotal_Location.X, PointFactor(Lbl_Subtotal_Location.Y, 0.935));
                lbl_Descuento.Location = new Point(lbl_Descuento_Location.X, PointFactor(lbl_Descuento_Location.Y, 0.935));
                Lbl_Impuesto.Location = new Point(Lbl_Impuesto_Location.X, PointFactor(Lbl_Impuesto_Location.Y, 0.935));
                lbl_Donacion.Location = new Point(lbl_Donacion_Location.X, PointFactor(lbl_Donacion_Location.Y, 0.935));
                Subtotal.Location = new Point(Subtotal_Location.X, PointFactor(Subtotal_Location.Y, 0.935));
                Impuesto.Location = new Point(Impuesto_Location.X, PointFactor(Impuesto_Location.Y, 0.935));
                Descuento.Location = new Point(Descuento_Location.X, PointFactor(Descuento_Location.Y, 0.935));
                Donacion.Location = new Point(Donacion_Location.X, PointFactor(Donacion_Location.Y, 0.935));

                Panel3.Location = new Point(Panel3_Location.X, PointFactor(Panel3_Location.Y, 0.9183));
                Panel3.Size = new Size(Panel3_Size.Width, PointFactor(Panel3_Size.Height, 1.4286));
                lblTotAlt1.Location = new Point(Panel3.Size.Width / 2, PointFactor(Total_Location.Y, 2));
                lblTotAlt2.Location = new Point(0, PointFactor(Total_Location.Y, 2));
                lblTotAlt1.Size = new Size(Panel3.Size.Width / 2, Total.Size.Height);
                lblTotAlt2.Size = new Size(Panel3.Size.Width / 2, Total.Size.Height);

                Panel2.Location = new Point(Panel2_Location.X, PointFactor(Panel2_Location.Y, 1.0446));
                Panel2.Size = new Size(Panel2_Size.Width, PointFactor(Panel2_Size.Height, 0.6875));
                Lbl_Peso.Location = new Point(Lbl_Peso_Location.X, PointFactor(Lbl_Peso_Location.Y, 0.2222));
                Peso.Location = new Point(Peso_Location.X, PointFactor(Peso_Location.Y, 0.2222));
                LogoBz.Visible = false;
            }
            else if (Math.Abs(CurrentScreenSize.Width - 1366) <= 10 && Math.Abs(CurrentScreenSize.Height - 768) <= 10)
            {

                // 1366x768

                Panel1.Size = new Size(Panel1_Size.Width, PointFactor(Panel1_Size.Height, 0.915));
                //LogoBz.Location = new Point(LogoBz_Location.X, PointFactor(LogoBz_Location.Y, -0.55));

                Lbl_Subtotal.Location = new Point(Lbl_Subtotal_Location.X, PointFactor(Lbl_Subtotal_Location.Y, 0.905));
                lbl_Descuento.Location = new Point(lbl_Descuento_Location.X, PointFactor(lbl_Descuento_Location.Y, 0.905));
                Lbl_Impuesto.Location = new Point(Lbl_Impuesto_Location.X, PointFactor(Lbl_Impuesto_Location.Y, 0.905));
                lbl_Donacion.Location = new Point(lbl_Donacion_Location.X, PointFactor(lbl_Donacion_Location.Y, 0.905));
                Subtotal.Location = new Point(Subtotal_Location.X, PointFactor(Subtotal_Location.Y, 0.905));
                Impuesto.Location = new Point(Impuesto_Location.X, PointFactor(Impuesto_Location.Y, 0.905));
                Descuento.Location = new Point(Descuento_Location.X, PointFactor(Descuento_Location.Y, 0.905));
                Donacion.Location = new Point(Donacion_Location.X, PointFactor(Donacion_Location.Y, 0.905));

                Panel3.Location = new Point(Panel3_Location.X, PointFactor(Panel3_Location.Y, 0.9183));
                Panel3.Size = new Size(Panel3_Size.Width, PointFactor(Panel3_Size.Height, 1.4286));
                lblTotAlt1.Location = new Point(Panel3.Size.Width / 2, PointFactor(Total_Location.Y, 2));
                lblTotAlt2.Location = new Point(0, PointFactor(Total_Location.Y, 2));
                lblTotAlt1.Size = new Size(Panel3.Size.Width / 2, Total.Size.Height);
                lblTotAlt2.Size = new Size(Panel3.Size.Width / 2, Total.Size.Height);

                Panel2.Location = new Point(Panel2_Location.X, PointFactor(Panel2_Location.Y, 1.0446));
                Panel2.Size = new Size(Panel2_Size.Width, PointFactor(Panel2_Size.Height, 0.6875));
                Lbl_Peso.Location = new Point(Lbl_Peso_Location.X, PointFactor(Lbl_Peso_Location.Y, 0.2222));
                Peso.Location = new Point(Peso_Location.X, PointFactor(Peso_Location.Y, 0.2222));

            }
            else if (Math.Abs(CurrentScreenSize.Width - 1024) <= 10 && Math.Abs(CurrentScreenSize.Height - 768) <= 10)
            {

                // 1024x768

                Panel1.Size = new Size(Panel1_Size.Width, PointFactor(Panel1_Size.Height, 0.915));
                //LogoBz.Location = new Point(LogoBz_Location.X, PointFactor(LogoBz_Location.Y, -0.55));

                Lbl_Subtotal.Location = new Point(Lbl_Subtotal_Location.X, PointFactor(Lbl_Subtotal_Location.Y, 0.905));
                lbl_Descuento.Location = new Point(lbl_Descuento_Location.X, PointFactor(lbl_Descuento_Location.Y, 0.905));
                Lbl_Impuesto.Location = new Point(Lbl_Impuesto_Location.X, PointFactor(Lbl_Impuesto_Location.Y, 0.905));
                lbl_Donacion.Location = new Point(lbl_Donacion_Location.X, PointFactor(lbl_Donacion_Location.Y, 0.905));
                Subtotal.Location = new Point(Subtotal_Location.X, PointFactor(Subtotal_Location.Y, 0.905));
                Impuesto.Location = new Point(Impuesto_Location.X, PointFactor(Impuesto_Location.Y, 0.905));
                Descuento.Location = new Point(Descuento_Location.X, PointFactor(Descuento_Location.Y, 0.905));
                Donacion.Location = new Point(Donacion_Location.X, PointFactor(Donacion_Location.Y, 0.905));

                Panel3.Location = new Point(Panel3_Location.X, PointFactor(Panel3_Location.Y, 0.9183));
                Panel3.Size = new Size(Panel3_Size.Width, PointFactor(Panel3_Size.Height, 1.4286));
                lblTotAlt1.Location = new Point(Panel3.Size.Width / 2, PointFactor(Total_Location.Y, 2));
                lblTotAlt2.Location = new Point(0, PointFactor(Total_Location.Y, 2));
                lblTotAlt1.Size = new Size(Panel3.Size.Width / 2, Total.Size.Height);
                lblTotAlt2.Size = new Size(Panel3.Size.Width / 2, Total.Size.Height);

                Panel2.Location = new Point(Panel2_Location.X, PointFactor(Panel2_Location.Y, 1.0446));
                Panel2.Size = new Size(Panel2_Size.Width, PointFactor(Panel2_Size.Height, 0.6875));
                Lbl_Peso.Location = new Point(Lbl_Peso_Location.X, PointFactor(Lbl_Peso_Location.Y, 0.2222));
                Lbl_Peso.Size = new Size(PointFactor(Lbl_Peso_Size.Width, 1.60), Lbl_Peso_Size.Height);
                Peso.Location = new Point(Peso_Location.X, PointFactor(Peso_Location.Y, 0.2222));

            }
            else if (Math.Abs(CurrentScreenSize.Width - 800) <= 10 && Math.Abs(CurrentScreenSize.Height - 600) <= 10)
            {

                // 800x600

                Panel1.Size = new Size(Panel1_Size.Width, PointFactor(Panel1_Size.Height, 0.915));
                //LogoBz.Location = new Point(LogoBz_Location.X, PointFactor(LogoBz_Location.Y, -0.55));

                Lbl_Subtotal.Location = new Point(Lbl_Subtotal_Location.X, PointFactor(Lbl_Subtotal_Location.Y, 0.905));
                lbl_Descuento.Location = new Point(lbl_Descuento_Location.X, PointFactor(lbl_Descuento_Location.Y, 0.905));
                Lbl_Impuesto.Location = new Point(Lbl_Impuesto_Location.X, PointFactor(Lbl_Impuesto_Location.Y, 0.905));
                lbl_Donacion.Location = new Point(lbl_Donacion_Location.X, PointFactor(lbl_Donacion_Location.Y, 0.905));
                Subtotal.Location = new Point(Subtotal_Location.X, PointFactor(Subtotal_Location.Y, 0.905));
                Impuesto.Location = new Point(Impuesto_Location.X, PointFactor(Impuesto_Location.Y, 0.905));
                Descuento.Location = new Point(Descuento_Location.X, PointFactor(Descuento_Location.Y, 0.905));
                Donacion.Location = new Point(Donacion_Location.X, PointFactor(Donacion_Location.Y, 0.905));

                Panel3.Location = new Point(Panel3_Location.X, PointFactor(Panel3_Location.Y, 0.9183));
                Panel3.Size = new Size(Panel3_Size.Width, PointFactor(Panel3_Size.Height, 1.4286));
                lblTotAlt1.Location = new Point(Panel3.Size.Width / 2, PointFactor(Total_Location.Y, 2));
                lblTotAlt2.Location = new Point(0, PointFactor(Total_Location.Y, 2));
                lblTotAlt1.Size = new Size(Panel3.Size.Width / 2, Total.Size.Height);
                lblTotAlt2.Size = new Size(Panel3.Size.Width / 2, Total.Size.Height);

                Panel2.Location = new Point(Panel2_Location.X, PointFactor(Panel2_Location.Y, 1.0446));
                Panel2.Size = new Size(Panel2_Size.Width, PointFactor(Panel2_Size.Height, 0.6875));
                Lbl_Peso.Location = new Point(Lbl_Peso_Location.X, PointFactor(Lbl_Peso_Location.Y, 0.2222));
                Lbl_Peso.Size = new Size(PointFactor(Lbl_Peso_Size.Width, 1.60), Lbl_Peso_Size.Height);
                Peso.Location = new Point(Peso_Location.X, PointFactor(Peso_Location.Y, 0.2222));

            }

            //Panel1.Size = new Size(Panel1_Size.Width, PointFactor(Panel1_Size.Height, 0.9153));
            //LogoBz.Location = new Point(LogoBz_Location.X, PointFactor(LogoBz_Location.Y, -0.5385));

            //Lbl_Subtotal.Location = new Point(Lbl_Subtotal_Location.X, PointFactor(Lbl_Subtotal_Location.Y, 0.8125));
            //lbl_Descuento.Location = new Point(lbl_Descuento_Location.X, PointFactor(lbl_Descuento_Location.Y, 0.8125));
            //Lbl_Impuesto.Location = new Point(Lbl_Impuesto_Location.X, PointFactor(Lbl_Impuesto_Location.Y, 0.8125));
            //lbl_Donacion.Location = new Point(lbl_Donacion_Location.X, PointFactor(lbl_Donacion_Location.Y, 0.8125));
            //Subtotal.Location = new Point(Subtotal_Location.X, PointFactor(Subtotal_Location.Y, 0.8));
            //Impuesto.Location = new Point(Impuesto_Location.X, PointFactor(Impuesto_Location.Y, 0.8));
            //Descuento.Location = new Point(Descuento_Location.X, PointFactor(Descuento_Location.Y, 0.8));
            //Donacion.Location = new Point(Donacion_Location.X, PointFactor(Donacion_Location.Y, 0.8));

            //Panel3.Location = new Point(Panel3_Location.X, PointFactor(Panel3_Location.Y, 0.9183));
            //Panel3.Size = new Size(Panel3_Size.Width, PointFactor(Panel3_Size.Height, 1.4286));
            //lblTotAlt1.Location = new Point(Panel3.Size.Width / 2, PointFactor(Total_Location.Y, 1.9268));
            //lblTotAlt2.Location = new Point(0, PointFactor(Total_Location.Y, 1.9268));
            //lblTotAlt1.Size = new Size(Panel3.Size.Width / 2, Total.Size.Height);
            //lblTotAlt2.Size = new Size(Panel3.Size.Width / 2, Total.Size.Height);

            //Panel2.Location = new Point(Panel2_Location.X, PointFactor(Panel2_Location.Y, 1.0446));
            //Panel2.Size = new Size(Panel2_Size.Width, PointFactor(Panel2_Size.Height, 0.6875));
            //Lbl_Peso.Location = new Point(Lbl_Peso_Location.X, PointFactor(Lbl_Peso_Location.Y, 0.2222));
            //Peso.Location = new Point(Peso_Location.X, PointFactor(Peso_Location.Y, 0.2222));
            ////Peso.Size = new Size(Peso_Size.Width, PointFactor(Peso_Size.Height, 0.9153));

            //Panel1.Size = new Size(356, 216);

            //LogoBz.Location = new Point(54, -7);
            //LogoBz.Size = new Size(236, 100);

            //Lbl_Subtotal.Location = new Point(14, 104);
            //lbl_Descuento.Location = new Point(14, 133);
            //Lbl_Impuesto.Location = new Point(14, 164);
            //lbl_Donacion.Location = new Point(14, 190);
            //Subtotal.Location = new Point(116, 96);
            //Impuesto.Location = new Point(116, 125);
            //Descuento.Location = new Point(116, 156);
            //Donacion.Location = new Point(115, 182);

            //Panel3.Size = new Size(356, 120);
            //lblTotAlt1.Location = new Point(178, 80);
            //lblTotAlt2.Location = new Point(0, 80);

            //Panel2.Location = new Point(1000, 350);
            //Panel2.Size = new Size(356, 33);
            //Lbl_Peso.Location = new Point(14, 5);
            //Peso.Location = new Point(190, 3);
            //Peso.Size = new Size(150, 30);

            Panel3.BackColor = Panel1.BackColor;
            Lbl_Total.BackColor = Panel3.BackColor;
            Total.BackColor = Panel3.BackColor;
            lblTotAlt1.BackColor = Panel3.BackColor;
            lblTotAlt2.BackColor = Panel3.BackColor;
            if (Properties.Settings.Default.ModoFood)
            {
                Lbl_Total.ForeColor = Color.FromArgb(1, 244, 126, 32);
                Total.ForeColor = Color.FromArgb(1, 244, 126, 32);
            }
            else
            {
                Lbl_Total.ForeColor = System.Drawing.Color.Firebrick;
                Total.ForeColor = System.Drawing.Color.Firebrick;
            }
            //Lbl_Total.ForeColor = System.Drawing.Color.Firebrick;
            //Total.ForeColor = System.Drawing.Color.Firebrick;

            lblTotAlt1.ForeColor = Color.FromArgb(0, 128, 0);
            lblTotAlt2.ForeColor = Color.FromArgb(0, 0, 192);

        }

        private void lblTotAlt2_Click(object sender, EventArgs e)
        {

        }

        private void lblTotAlt2_TextChanged(object sender, EventArgs e)
        {
            RedimensionarTotal_Half(lblTotAlt2);
        }

        private void lblTotAlt1_Click(object sender, EventArgs e)
        {

        }

        private void lblTotAlt1_TextChanged(object sender, EventArgs e)
        {
            RedimensionarTotal_Half(lblTotAlt1);
        }

        private void Total_TextChanged(object sender, EventArgs e)
        {
            RedimensionarTotal(Total);
        }

        private void Subtotal_Click(object sender, EventArgs e)
        {

        }

        private void Descuento_Click(object sender, EventArgs e)
        {
           
        }

        private void Impuesto_Click(object sender, EventArgs e)
        {
         
        }

        private void Donacion_Click(object sender, EventArgs e)
        {
           
        }

        private void Total_Click_1(object sender, EventArgs e)
        {

        }

        private void RedimensionarSubtotales(Label pLabel)
        {
            if (Math.Abs(CurrentScreenSize.Width - 1920) <= 10 && Math.Abs(CurrentScreenSize.Height - 1080) <= 10)
            {
                // 1920x1080
                // Ajustar 1
                if (pLabel.Text.Length <= 12)
                    pLabel.Font = new Font(pLabel.Font.Name, 26F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 13)
                    pLabel.Font = new Font(pLabel.Font.Name, 24.5F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 14)
                    pLabel.Font = new Font(pLabel.Font.Name, 24F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 15)
                    pLabel.Font = new Font(pLabel.Font.Name, 22.5F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 16)
                    pLabel.Font = new Font(pLabel.Font.Name, 21F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 17)
                    pLabel.Font = new Font(pLabel.Font.Name, 19.5F, pLabel.Font.Style, pLabel.Font.Unit);
                else// if (pLabel.Text.Length <= 18)
                    pLabel.Font = new Font(pLabel.Font.Name, 18F, pLabel.Font.Style, pLabel.Font.Unit);
            }
            else if (Math.Abs(CurrentScreenSize.Width - 1366) <= 10 && Math.Abs(CurrentScreenSize.Height - 768) <= 10)
            {
                // 1366x768
                // No hacer nada. Por defecto se ve bien.
                if (pLabel.Text.Length <= 13)
                    pLabel.Font = new Font(pLabel.Font.Name, 18F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 14)
                    pLabel.Font = new Font(pLabel.Font.Name, 16.5F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 16)
                    pLabel.Font = new Font(pLabel.Font.Name, 15F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 17)
                    pLabel.Font = new Font(pLabel.Font.Name, 14F, pLabel.Font.Style, pLabel.Font.Unit);
                else// if (pLabel.Text.Length <= 18)
                    pLabel.Font = new Font(pLabel.Font.Name, 13F, pLabel.Font.Style, pLabel.Font.Unit);
            }
            else if (Math.Abs(CurrentScreenSize.Width - 1024) <= 10 && Math.Abs(CurrentScreenSize.Height - 768) <= 10)
            {
                // 1024x768
                // Ajustar 2
                if (pLabel.Text.Length <= 9)
                    pLabel.Font = new Font(pLabel.Font.Name, 18F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 10)
                    pLabel.Font = new Font(pLabel.Font.Name, 16.5F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 11)
                    pLabel.Font = new Font(pLabel.Font.Name, 15.5F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 13)
                    pLabel.Font = new Font(pLabel.Font.Name, 14F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 14)
                    pLabel.Font = new Font(pLabel.Font.Name, 12.5F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 16)
                    pLabel.Font = new Font(pLabel.Font.Name, 11F, pLabel.Font.Style, pLabel.Font.Unit);
                else// if (pLabel.Text.Length <= 18)
                    pLabel.Font = new Font(pLabel.Font.Name, 10F, pLabel.Font.Style, pLabel.Font.Unit);
            }
            else if (Math.Abs(CurrentScreenSize.Width - 800) <= 10 && Math.Abs(CurrentScreenSize.Height - 600) <= 10)
            {
                // 800x600
                // Ajustar 3
                if (pLabel.Text.Length <= 9)
                    pLabel.Font = new Font(pLabel.Font.Name, 15F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 10)
                    pLabel.Font = new Font(pLabel.Font.Name, 14F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 11)
                    pLabel.Font = new Font(pLabel.Font.Name, 12.5F, pLabel.Font.Style, pLabel.Font.Unit);
                else// if (pLabel.Text.Length <= 12)
                    pLabel.Font = new Font(pLabel.Font.Name, 10.5F, pLabel.Font.Style, pLabel.Font.Unit);
            }
        }

        private void RedimensionarTotal(Label pLabel)
        {
            if (Math.Abs(CurrentScreenSize.Width - 1920) <= 10 && Math.Abs(CurrentScreenSize.Height - 1080) <= 10)
            {
                // 1920x1080
                // Ajustar 1
                if (pLabel.Text.Length <= 15)
                    pLabel.Font = new Font(pLabel.Font.Name, 40F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 16)
                    pLabel.Font = new Font(pLabel.Font.Name, 39F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 17)
                    pLabel.Font = new Font(pLabel.Font.Name, 36F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 18)
                    pLabel.Font = new Font(pLabel.Font.Name, 34F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 19)
                    pLabel.Font = new Font(pLabel.Font.Name, 32F, pLabel.Font.Style, pLabel.Font.Unit);
                else// if (pLabel.Text.Length <= 20)
                    pLabel.Font = new Font(pLabel.Font.Name, 30.5F, pLabel.Font.Style, pLabel.Font.Unit);
            }
            else if (Math.Abs(CurrentScreenSize.Width - 1366) <= 10 && Math.Abs(CurrentScreenSize.Height - 768) <= 10)
            {
                // 1366x768
                if (pLabel.Text.Length <= 15)
                    pLabel.Font = new Font(pLabel.Font.Name, 30F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 16)
                    pLabel.Font = new Font(pLabel.Font.Name, 27F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 17)
                    pLabel.Font = new Font(pLabel.Font.Name, 25F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 18)
                    pLabel.Font = new Font(pLabel.Font.Name, 24F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 19)
                    pLabel.Font = new Font(pLabel.Font.Name, 23F, pLabel.Font.Style, pLabel.Font.Unit);
                else// if (pLabel.Text.Length <= 20)
                    pLabel.Font = new Font(pLabel.Font.Name, 21.5F, pLabel.Font.Style, pLabel.Font.Unit);
            }
            else if (Math.Abs(CurrentScreenSize.Width - 1024) <= 10 && Math.Abs(CurrentScreenSize.Height - 768) <= 10)
            {
                // 1024x768
                if (pLabel.Text.Length <= 11)
                    pLabel.Font = new Font(pLabel.Font.Name, 30F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 12)
                    pLabel.Font = new Font(pLabel.Font.Name, 27F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 13)
                    pLabel.Font = new Font(pLabel.Font.Name, 24.5F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 14)
                    pLabel.Font = new Font(pLabel.Font.Name, 23F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 15)
                    pLabel.Font = new Font(pLabel.Font.Name, 21.5F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 16)
                    pLabel.Font = new Font(pLabel.Font.Name, 21F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 17)
                    pLabel.Font = new Font(pLabel.Font.Name, 19.5F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 18)
                    pLabel.Font = new Font(pLabel.Font.Name, 18F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 19)
                    pLabel.Font = new Font(pLabel.Font.Name, 17F, pLabel.Font.Style, pLabel.Font.Unit);
                else// if (pLabel.Text.Length <= 20)
                    pLabel.Font = new Font(pLabel.Font.Name, 15.5F, pLabel.Font.Style, pLabel.Font.Unit);
            }
            else if (Math.Abs(CurrentScreenSize.Width - 800) <= 10 && Math.Abs(CurrentScreenSize.Height - 600) <= 10)
            {
                // 800x600
                if (pLabel.Text.Length <= 10)
                    pLabel.Font = new Font(pLabel.Font.Name, 24F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 11)
                    pLabel.Font = new Font(pLabel.Font.Name, 23F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 12)
                    pLabel.Font = new Font(pLabel.Font.Name, 21F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 13)
                    pLabel.Font = new Font(pLabel.Font.Name, 19.5F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 14)
                    pLabel.Font = new Font(pLabel.Font.Name, 18F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 15)
                    pLabel.Font = new Font(pLabel.Font.Name, 17F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 16)
                    pLabel.Font = new Font(pLabel.Font.Name, 15.5F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 18)
                    pLabel.Font = new Font(pLabel.Font.Name, 14F, pLabel.Font.Style, pLabel.Font.Unit);
                else// if (pLabel.Text.Length <= 20)
                    pLabel.Font = new Font(pLabel.Font.Name, 12.5F, pLabel.Font.Style, pLabel.Font.Unit);
            }
        }
        private void RedimensionarTotal_Half(Label pLabel)
        {
            if (Math.Abs(CurrentScreenSize.Width - 1920) <= 10 && Math.Abs(CurrentScreenSize.Height - 1080) <= 10)
            {
                // 1920x1080
                if (pLabel.Text.Length <= 7)
                    pLabel.Font = new Font(pLabel.Font.Name, 36F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 8)
                    pLabel.Font = new Font(pLabel.Font.Name, 35.5F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 9)
                    pLabel.Font = new Font(pLabel.Font.Name, 33.5F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 10)
                    pLabel.Font = new Font(pLabel.Font.Name, 30.5F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 11)
                    pLabel.Font = new Font(pLabel.Font.Name, 28.5F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 12)
                    pLabel.Font = new Font(pLabel.Font.Name, 25.5F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 13)
                    pLabel.Font = new Font(pLabel.Font.Name, 23F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 14)
                    pLabel.Font = new Font(pLabel.Font.Name, 21.5F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 15)
                    pLabel.Font = new Font(pLabel.Font.Name, 21F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 16)
                    pLabel.Font = new Font(pLabel.Font.Name, 19.5F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 17)
                    pLabel.Font = new Font(pLabel.Font.Name, 18F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 18)
                    pLabel.Font = new Font(pLabel.Font.Name, 17F, pLabel.Font.Style, pLabel.Font.Unit);
                else// if (pLabel.Text.Length <= 20)
                    pLabel.Font = new Font(pLabel.Font.Name, 15.5F, pLabel.Font.Style, pLabel.Font.Unit);
            }
            else if (Math.Abs(CurrentScreenSize.Width - 1366) <= 10 && Math.Abs(CurrentScreenSize.Height - 768) <= 10)
            {
                // 1366x768
                if (pLabel.Text.Length <= 7)
                    pLabel.Font = new Font(pLabel.Font.Name, 27F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 8)
                    pLabel.Font = new Font(pLabel.Font.Name, 26.5F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 9)
                    pLabel.Font = new Font(pLabel.Font.Name, 24.5F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 10)
                    pLabel.Font = new Font(pLabel.Font.Name, 21.5F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 11)
                    pLabel.Font = new Font(pLabel.Font.Name, 19.5F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 12)
                    pLabel.Font = new Font(pLabel.Font.Name, 18F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 13)
                    pLabel.Font = new Font(pLabel.Font.Name, 17F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 14)
                    pLabel.Font = new Font(pLabel.Font.Name, 15.5F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 15)
                    pLabel.Font = new Font(pLabel.Font.Name, 14F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 16)
                    pLabel.Font = new Font(pLabel.Font.Name, 12.5F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 17)
                    pLabel.Font = new Font(pLabel.Font.Name, 12F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 18)
                    pLabel.Font = new Font(pLabel.Font.Name, 11.5F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 19)
                    pLabel.Font = new Font(pLabel.Font.Name, 11F, pLabel.Font.Style, pLabel.Font.Unit);
                else// if (pLabel.Text.Length <= 20)
                    pLabel.Font = new Font(pLabel.Font.Name, 10.5F, pLabel.Font.Style, pLabel.Font.Unit);
            }
            else if (Math.Abs(CurrentScreenSize.Width - 1024) <= 10 && Math.Abs(CurrentScreenSize.Height - 768) <= 10)
            {
                // 1024x768
                if (pLabel.Text.Length <= 5)
                    pLabel.Font = new Font(pLabel.Font.Name, 30F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 6)
                    pLabel.Font = new Font(pLabel.Font.Name, 27F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 7)
                    pLabel.Font = new Font(pLabel.Font.Name, 23F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 8)
                    pLabel.Font = new Font(pLabel.Font.Name, 19F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 9)
                    pLabel.Font = new Font(pLabel.Font.Name, 18F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 10)
                    pLabel.Font = new Font(pLabel.Font.Name, 15.5F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 11)
                    pLabel.Font = new Font(pLabel.Font.Name, 14F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 12)
                    pLabel.Font = new Font(pLabel.Font.Name, 12.5F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 13)
                    pLabel.Font = new Font(pLabel.Font.Name, 12F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 14)
                    pLabel.Font = new Font(pLabel.Font.Name, 11.5F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 15)
                    pLabel.Font = new Font(pLabel.Font.Name, 10.5F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 16)
                    pLabel.Font = new Font(pLabel.Font.Name, 10F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 17)
                    pLabel.Font = new Font(pLabel.Font.Name, 9.5F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 18)
                    pLabel.Font = new Font(pLabel.Font.Name, 9F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 19)
                    pLabel.Font = new Font(pLabel.Font.Name, 8.5F, pLabel.Font.Style, pLabel.Font.Unit);
                else// if (pLabel.Text.Length <= 20)
                    pLabel.Font = new Font(pLabel.Font.Name, 8F, pLabel.Font.Style, pLabel.Font.Unit);
            }
            else if (Math.Abs(CurrentScreenSize.Width - 800) <= 10 && Math.Abs(CurrentScreenSize.Height - 600) <= 10)
            {
                // 800x600
                if (pLabel.Text.Length <= 5)
                    pLabel.Font = new Font(pLabel.Font.Name, 24F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 6)
                    pLabel.Font = new Font(pLabel.Font.Name, 21F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 7)
                    pLabel.Font = new Font(pLabel.Font.Name, 18F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 8)
                    pLabel.Font = new Font(pLabel.Font.Name, 15.5F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 9)
                    pLabel.Font = new Font(pLabel.Font.Name, 14F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 10)
                    pLabel.Font = new Font(pLabel.Font.Name, 12.5F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 11)
                    pLabel.Font = new Font(pLabel.Font.Name, 12F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 12)
                    pLabel.Font = new Font(pLabel.Font.Name, 10.5F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 13)
                    pLabel.Font = new Font(pLabel.Font.Name, 9F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 14)
                    pLabel.Font = new Font(pLabel.Font.Name, 8.5F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 15)
                    pLabel.Font = new Font(pLabel.Font.Name, 8F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 16)
                    pLabel.Font = new Font(pLabel.Font.Name, 7.5F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 17)
                    pLabel.Font = new Font(pLabel.Font.Name, 7F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 18)
                    pLabel.Font = new Font(pLabel.Font.Name, 6.5F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 19)
                    pLabel.Font = new Font(pLabel.Font.Name, 6F, pLabel.Font.Style, pLabel.Font.Unit);
                else// if (pLabel.Text.Length <= 20)
                    pLabel.Font = new Font(pLabel.Font.Name, 5.5F, pLabel.Font.Style, pLabel.Font.Unit);
            }
        }

        private void Subtotal_TextChanged(object sender, EventArgs e)
        {
            RedimensionarSubtotales(Subtotal);
        }

        private void Descuento_TextChanged(object sender, EventArgs e)
        {
            RedimensionarSubtotales(Descuento);
        }

        private void Impuesto_TextChanged(object sender, EventArgs e)
        {
            RedimensionarSubtotales(Impuesto);
        }

        private void Donacion_TextChanged(object sender, EventArgs e)
        {
            RedimensionarSubtotales(Donacion);
        }

        private void MontoIGTF_TextChanged(object sender, EventArgs e)
        {
            RedimensionarSubtotales(MontoIGTF);
        }

        private void RedimensionarGranTotal_Full(Label pLabel)
        {
            if (Math.Abs(CurrentScreenSize.Width - 1920) <= 10 && Math.Abs(CurrentScreenSize.Height - 1080) <= 10)
            {
                // 1920x1080
                if (pLabel.Text.Length <= 10)
                    pLabel.Font = new Font(pLabel.Font.Name, 240F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 11)
                    pLabel.Font = new Font(pLabel.Font.Name, 220F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 12)
                    pLabel.Font = new Font(pLabel.Font.Name, 200F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 13)
                    pLabel.Font = new Font(pLabel.Font.Name, 180F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 14)
                    pLabel.Font = new Font(pLabel.Font.Name, 170F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 15)
                    pLabel.Font = new Font(pLabel.Font.Name, 155F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 16)
                    pLabel.Font = new Font(pLabel.Font.Name, 140F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 17)
                    pLabel.Font = new Font(pLabel.Font.Name, 130F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 18)
                    pLabel.Font = new Font(pLabel.Font.Name, 120F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 19)
                    pLabel.Font = new Font(pLabel.Font.Name, 115F, pLabel.Font.Style, pLabel.Font.Unit);
                else// if (pLabel.Text.Length <= 20)
                    pLabel.Font = new Font(pLabel.Font.Name, 110F, pLabel.Font.Style, pLabel.Font.Unit);
            }
            else if (Math.Abs(CurrentScreenSize.Width - 1366) <= 10 && Math.Abs(CurrentScreenSize.Height - 768) <= 10)
            {
                // 1366x768
                if (pLabel.Text.Length <= 10)
                    pLabel.Font = new Font(pLabel.Font.Name, 170F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 11)
                    pLabel.Font = new Font(pLabel.Font.Name, 150F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 12)
                    pLabel.Font = new Font(pLabel.Font.Name, 140F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 13)
                    pLabel.Font = new Font(pLabel.Font.Name, 130F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 14)
                    pLabel.Font = new Font(pLabel.Font.Name, 120F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 15)
                    pLabel.Font = new Font(pLabel.Font.Name, 110F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 16)
                    pLabel.Font = new Font(pLabel.Font.Name, 100F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 17)
                    pLabel.Font = new Font(pLabel.Font.Name, 95F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 18)
                    pLabel.Font = new Font(pLabel.Font.Name, 90F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 19)
                    pLabel.Font = new Font(pLabel.Font.Name, 85F, pLabel.Font.Style, pLabel.Font.Unit);
                else// if (pLabel.Text.Length <= 20)
                    pLabel.Font = new Font(pLabel.Font.Name, 80F, pLabel.Font.Style, pLabel.Font.Unit);
            }
            else if (Math.Abs(CurrentScreenSize.Width - 1024) <= 10 && Math.Abs(CurrentScreenSize.Height - 768) <= 10)
            {
                // 1024x768
                if (pLabel.Text.Length <= 8)
                    pLabel.Font = new Font(pLabel.Font.Name, 140F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 9)
                    pLabel.Font = new Font(pLabel.Font.Name, 135F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 10)
                    pLabel.Font = new Font(pLabel.Font.Name, 130F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 11)
                    pLabel.Font = new Font(pLabel.Font.Name, 125F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 12)
                    pLabel.Font = new Font(pLabel.Font.Name, 115F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 13)
                    pLabel.Font = new Font(pLabel.Font.Name, 100F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 14)
                    pLabel.Font = new Font(pLabel.Font.Name, 90F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 15)
                    pLabel.Font = new Font(pLabel.Font.Name, 85F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 16)
                    pLabel.Font = new Font(pLabel.Font.Name, 80F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 17)
                    pLabel.Font = new Font(pLabel.Font.Name, 75F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 18)
                    pLabel.Font = new Font(pLabel.Font.Name, 70F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 19)
                    pLabel.Font = new Font(pLabel.Font.Name, 65F, pLabel.Font.Style, pLabel.Font.Unit);
                else// if (pLabel.Text.Length <= 20)
                    pLabel.Font = new Font(pLabel.Font.Name, 60F, pLabel.Font.Style, pLabel.Font.Unit);
            }
            else if (Math.Abs(CurrentScreenSize.Width - 800) <= 10 && Math.Abs(CurrentScreenSize.Height - 600) <= 10)
            {
                // 800x600
                if (pLabel.Text.Length <= 8)
                    pLabel.Font = new Font(pLabel.Font.Name, 130F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 9)
                    pLabel.Font = new Font(pLabel.Font.Name, 115F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 10)
                    pLabel.Font = new Font(pLabel.Font.Name, 105F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 11)
                    pLabel.Font = new Font(pLabel.Font.Name, 95F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 12)
                    pLabel.Font = new Font(pLabel.Font.Name, 85F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 13)
                    pLabel.Font = new Font(pLabel.Font.Name, 80F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 14)
                    pLabel.Font = new Font(pLabel.Font.Name, 75F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 15)
                    pLabel.Font = new Font(pLabel.Font.Name, 70F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 16)
                    pLabel.Font = new Font(pLabel.Font.Name, 65F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 17)
                    pLabel.Font = new Font(pLabel.Font.Name, 60F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 18)
                    pLabel.Font = new Font(pLabel.Font.Name, 55F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 19)
                    pLabel.Font = new Font(pLabel.Font.Name, 52.5F, pLabel.Font.Style, pLabel.Font.Unit);
                else// if (pLabel.Text.Length <= 20)
                    pLabel.Font = new Font(pLabel.Font.Name, 50F, pLabel.Font.Style, pLabel.Font.Unit);
            }
        }

        private void RedimensionarGranTotal_Half(Label pLabel)
        {
            if (Math.Abs(CurrentScreenSize.Width - 1920) <= 10 && Math.Abs(CurrentScreenSize.Height - 1080) <= 10)
            {
                // 1920x1080
                //if (pLabel.Text.Length <= 14)
                    //pLabel.Font = new Font(pLabel.Font.Name, 180F, pLabel.Font.Style, pLabel.Font.Unit);
                //else if (pLabel.Text.Length <= 15)
                    //pLabel.Font = new Font(pLabel.Font.Name, 170F, pLabel.Font.Style, pLabel.Font.Unit);
                //else if (pLabel.Text.Length <= 16)
                    //pLabel.Font = new Font(pLabel.Font.Name, 160F, pLabel.Font.Style, pLabel.Font.Unit);
                //else if (pLabel.Text.Length <= 17)
                if (pLabel.Text.Length <= 17)
                    //pLabel.Font = new Font(pLabel.Font.Name, 150F, pLabel.Font.Style, pLabel.Font.Unit);
                    pLabel.Font = new Font(pLabel.Font.Name, 145F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 18)
                    pLabel.Font = new Font(pLabel.Font.Name, 140F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 19)
                    pLabel.Font = new Font(pLabel.Font.Name, 130F, pLabel.Font.Style, pLabel.Font.Unit);
                else// if (pLabel.Text.Length <= 20)
                    pLabel.Font = new Font(pLabel.Font.Name, 125F, pLabel.Font.Style, pLabel.Font.Unit);
            }
            else if (Math.Abs(CurrentScreenSize.Width - 1366) <= 10 && Math.Abs(CurrentScreenSize.Height - 768) <= 10)
            {
                // 1366x768
                //if (pLabel.Text.Length <= 14)
                    //pLabel.Font = new Font(pLabel.Font.Name, 125F, pLabel.Font.Style, pLabel.Font.Unit);
                //else if (pLabel.Text.Length <= 15)
                    //pLabel.Font = new Font(pLabel.Font.Name, 120F, pLabel.Font.Style, pLabel.Font.Unit);
                //else if (pLabel.Text.Length <= 16)
                    //pLabel.Font = new Font(pLabel.Font.Name, 115F, pLabel.Font.Style, pLabel.Font.Unit);
                //else if (pLabel.Text.Length <= 17)
                 if (pLabel.Text.Length <= 17)
                    pLabel.Font = new Font(pLabel.Font.Name, 105F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 18)
                    pLabel.Font = new Font(pLabel.Font.Name, 100F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 19)
                    pLabel.Font = new Font(pLabel.Font.Name, 95F, pLabel.Font.Style, pLabel.Font.Unit);
                else// if (pLabel.Text.Length <= 20)
                    pLabel.Font = new Font(pLabel.Font.Name, 90F, pLabel.Font.Style, pLabel.Font.Unit);
            }
            else if (Math.Abs(CurrentScreenSize.Width - 1024) <= 10 && Math.Abs(CurrentScreenSize.Height - 768) <= 10)
            {
                // 1024x768
                //if (pLabel.Text.Length <= 11)
                    //pLabel.Font = new Font(pLabel.Font.Name, 125F, pLabel.Font.Style, pLabel.Font.Unit);
                //else if (pLabel.Text.Length <= 12)
                    //pLabel.Font = new Font(pLabel.Font.Name, 115F, pLabel.Font.Style, pLabel.Font.Unit);
                //else if (pLabel.Text.Length <= 13)
                 if (pLabel.Text.Length <= 13)
                    pLabel.Font = new Font(pLabel.Font.Name, 105F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 14)
                    pLabel.Font = new Font(pLabel.Font.Name, 95F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 15)
                    pLabel.Font = new Font(pLabel.Font.Name, 90F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 16)
                    pLabel.Font = new Font(pLabel.Font.Name, 85F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 17)
                    pLabel.Font = new Font(pLabel.Font.Name, 80F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 18)
                    pLabel.Font = new Font(pLabel.Font.Name, 75F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 19)
                    pLabel.Font = new Font(pLabel.Font.Name, 70F, pLabel.Font.Style, pLabel.Font.Unit);
                else// if (pLabel.Text.Length <= 20)
                    pLabel.Font = new Font(pLabel.Font.Name, 65F, pLabel.Font.Style, pLabel.Font.Unit);
            }
            else if (Math.Abs(CurrentScreenSize.Width - 800) <= 10 && Math.Abs(CurrentScreenSize.Height - 600) <= 10)
            {
                // 800x600
                //if (pLabel.Text.Length <= 8)
                    //pLabel.Font = new Font(pLabel.Font.Name, 130F, pLabel.Font.Style, pLabel.Font.Unit);
                //else if (pLabel.Text.Length <= 9)
                    //pLabel.Font = new Font(pLabel.Font.Name, 115F, pLabel.Font.Style, pLabel.Font.Unit);
                //else if (pLabel.Text.Length <= 10)
                    //pLabel.Font = new Font(pLabel.Font.Name, 105F, pLabel.Font.Style, pLabel.Font.Unit);
                //else if (pLabel.Text.Length <= 11)
                    //pLabel.Font = new Font(pLabel.Font.Name, 95F, pLabel.Font.Style, pLabel.Font.Unit);
                //else if (pLabel.Text.Length <= 12)
                if (pLabel.Text.Length <= 12)
                    pLabel.Font = new Font(pLabel.Font.Name, 85F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 13)
                    pLabel.Font = new Font(pLabel.Font.Name, 80F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 14)
                    pLabel.Font = new Font(pLabel.Font.Name, 75F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 15)
                    pLabel.Font = new Font(pLabel.Font.Name, 70F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 16)
                    pLabel.Font = new Font(pLabel.Font.Name, 65F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 17)
                    pLabel.Font = new Font(pLabel.Font.Name, 60F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 18)
                    pLabel.Font = new Font(pLabel.Font.Name, 55F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 19)
                    pLabel.Font = new Font(pLabel.Font.Name, 52.5F, pLabel.Font.Style, pLabel.Font.Unit);
                else// if (pLabel.Text.Length <= 20)
                    pLabel.Font = new Font(pLabel.Font.Name, 50F, pLabel.Font.Style, pLabel.Font.Unit);
            }
        }

        private void RedimensionarGranTotal_Quarter(Label pLabel)
        {
            if (Math.Abs(CurrentScreenSize.Width - 1920) <= 10 && Math.Abs(CurrentScreenSize.Height - 1080) <= 10)
            {
                // 1920x1080
                if (pLabel.Text.Length <= 9)
                    pLabel.Font = new Font(pLabel.Font.Name, 140F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 10)
                    pLabel.Font = new Font(pLabel.Font.Name, 127F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 11)
                    pLabel.Font = new Font(pLabel.Font.Name, 114F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 12)
                    pLabel.Font = new Font(pLabel.Font.Name, 104F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 13)
                    pLabel.Font = new Font(pLabel.Font.Name, 95F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 14)
                    pLabel.Font = new Font(pLabel.Font.Name, 87F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 15)
                    pLabel.Font = new Font(pLabel.Font.Name, 81F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 16)
                    pLabel.Font = new Font(pLabel.Font.Name, 75F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 17)
                    pLabel.Font = new Font(pLabel.Font.Name, 69F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 18)
                    pLabel.Font = new Font(pLabel.Font.Name, 65F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 19)
                    pLabel.Font = new Font(pLabel.Font.Name, 62F, pLabel.Font.Style, pLabel.Font.Unit);
                else// if (pLabel.Text.Length <= 20)
                    pLabel.Font = new Font(pLabel.Font.Name, 58F, pLabel.Font.Style, pLabel.Font.Unit);
            }
            else if (Math.Abs(CurrentScreenSize.Width - 1366) <= 10 && Math.Abs(CurrentScreenSize.Height - 768) <= 10)
            {
                // 1366x768
                if (pLabel.Text.Length <= 8)
                    pLabel.Font = new Font(pLabel.Font.Name, 105F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 9)
                    pLabel.Font = new Font(pLabel.Font.Name, 99F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 10)
                    pLabel.Font = new Font(pLabel.Font.Name, 89F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 11)
                    pLabel.Font = new Font(pLabel.Font.Name, 80F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 12)
                    pLabel.Font = new Font(pLabel.Font.Name, 73F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 13)
                    pLabel.Font = new Font(pLabel.Font.Name, 66F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 14)
                    pLabel.Font = new Font(pLabel.Font.Name, 61F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 15)
                    pLabel.Font = new Font(pLabel.Font.Name, 56F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 16)
                    pLabel.Font = new Font(pLabel.Font.Name, 52F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 17)
                    pLabel.Font = new Font(pLabel.Font.Name, 49F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 18)
                    pLabel.Font = new Font(pLabel.Font.Name, 46F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 19)
                    pLabel.Font = new Font(pLabel.Font.Name, 44F, pLabel.Font.Style, pLabel.Font.Unit);
                else// if (pLabel.Text.Length <= 20)
                    pLabel.Font = new Font(pLabel.Font.Name, 42F, pLabel.Font.Style, pLabel.Font.Unit);
            }
            else if (Math.Abs(CurrentScreenSize.Width - 1024) <= 10 && Math.Abs(CurrentScreenSize.Height - 768) <= 10)
            {
                // 1024x768
                if (pLabel.Text.Length <= 6)
                    pLabel.Font = new Font(pLabel.Font.Name, 105F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 7)
                    pLabel.Font = new Font(pLabel.Font.Name, 97F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 8)
                    pLabel.Font = new Font(pLabel.Font.Name, 82F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 9)
                    pLabel.Font = new Font(pLabel.Font.Name, 74F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 10)
                    pLabel.Font = new Font(pLabel.Font.Name, 65F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 11)
                    pLabel.Font = new Font(pLabel.Font.Name, 60F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 13)
                    pLabel.Font = new Font(pLabel.Font.Name, 54F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 14)
                    pLabel.Font = new Font(pLabel.Font.Name, 50F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 15)
                    pLabel.Font = new Font(pLabel.Font.Name, 47F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 16)
                    pLabel.Font = new Font(pLabel.Font.Name, 44F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 17)
                    pLabel.Font = new Font(pLabel.Font.Name, 41F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 18)
                    pLabel.Font = new Font(pLabel.Font.Name, 35F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 19)
                    pLabel.Font = new Font(pLabel.Font.Name, 33F, pLabel.Font.Style, pLabel.Font.Unit);
                else// if (pLabel.Text.Length <= 20)
                    pLabel.Font = new Font(pLabel.Font.Name, 31F, pLabel.Font.Style, pLabel.Font.Unit);
            }
            else if (Math.Abs(CurrentScreenSize.Width - 800) <= 10 && Math.Abs(CurrentScreenSize.Height - 600) <= 10)
            {
                // 800x600
                if (pLabel.Text.Length <= 6)
                    pLabel.Font = new Font(pLabel.Font.Name, 83F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 7)
                    pLabel.Font = new Font(pLabel.Font.Name, 75F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 8)
                    pLabel.Font = new Font(pLabel.Font.Name, 65F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 9)
                    pLabel.Font = new Font(pLabel.Font.Name, 57F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 10)
                    pLabel.Font = new Font(pLabel.Font.Name, 51F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 11)
                    pLabel.Font = new Font(pLabel.Font.Name, 45F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 12)
                    pLabel.Font = new Font(pLabel.Font.Name, 42F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 13)
                    pLabel.Font = new Font(pLabel.Font.Name, 38F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 14)
                    pLabel.Font = new Font(pLabel.Font.Name, 40F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 15)
                    pLabel.Font = new Font(pLabel.Font.Name, 37F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 16)
                    pLabel.Font = new Font(pLabel.Font.Name, 34F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 17)
                    pLabel.Font = new Font(pLabel.Font.Name, 33F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 18)
                    pLabel.Font = new Font(pLabel.Font.Name, 30F, pLabel.Font.Style, pLabel.Font.Unit);
                else if (pLabel.Text.Length <= 19)
                    pLabel.Font = new Font(pLabel.Font.Name, 28F, pLabel.Font.Style, pLabel.Font.Unit);
                else// if (pLabel.Text.Length <= 20)
                    pLabel.Font = new Font(pLabel.Font.Name, 25F, pLabel.Font.Style, pLabel.Font.Unit);
            }
        }

        private void lblGranTotal_TextChanged(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.GranTotalAutoScaling)
            {
                if (Properties.Settings.Default.MonedaAdicional1AMostrar.isUndefined())
                    RedimensionarGranTotal_Full(lblGranTotal);
                else
                    RedimensionarGranTotal_Half(lblGranTotal);
            }
        }

        private void lblGranTotalAlt1_TextChanged(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.MonedaAdicional2AMostrar.isUndefined())
                RedimensionarGranTotal_Half(lblGranTotalAlt1);
            else
                RedimensionarGranTotal_Quarter(lblGranTotalAlt1);
        }

        private void lblGranTotalAlt2_TextChanged(object sender, EventArgs e)
        {
            RedimensionarGranTotal_Quarter(lblGranTotalAlt2);
        }

        private void txtTestLen1_TextChanged(object sender, EventArgs e)
        {
            //Double mValor = 0;
            //if (Double.TryParse(txtTestLen1.Text, out mValor))
            //{
            this.TimerUpdateDatas.Enabled = false;
            Subtotal.Text = txtTestLen1.Text;
            Total.Text = txtTestLen1.Text;
            lblTotAlt1.Text= txtTestLen1.Text;
            lblTotAlt2.Text = txtTestLen1.Text;
            //lblGranTotal.Text = txtTestLen1.Text;
            //lblGranTotalAlt1.Text = txtTestLen1.Text;
            //lblGranTotalAlt2.Text = txtTestLen1.Text;
            //}
        }

        private void txtTestFontSize1_TextChanged(object sender, EventArgs e)
        {
            float mValor = 0;
            if (float.TryParse(txtTestFontSize1.Text, out mValor))
            {
                if (mValor > 0)
                {
                    //Subtotal.Font = new Font(Subtotal.Font.Name, mValor, Subtotal.Font.Style, Subtotal.Font.Unit);
                    Total.Font = new Font(Total.Font.Name, mValor, Total.Font.Style, Total.Font.Unit);
                    lblTotAlt1.Font = new Font(lblTotAlt1.Font.Name, mValor, lblTotAlt1.Font.Style, lblTotAlt1.Font.Unit);
                    lblTotAlt2.Font = new Font(lblTotAlt2.Font.Name, mValor, lblTotAlt2.Font.Style, lblTotAlt2.Font.Unit);
                    //lblGranTotal.Font = new Font(lblGranTotal.Font.Name, mValor, lblGranTotal.Font.Style, lblGranTotal.Font.Unit);
                    //lblGranTotalAlt1.Font = new Font(lblGranTotalAlt1.Font.Name, mValor, lblGranTotalAlt1.Font.Style, lblGranTotalAlt1.Font.Unit);
                }
            }

        }

        private void MontoIGTF_Click(object sender, EventArgs e)
        {

        }

    }

}
