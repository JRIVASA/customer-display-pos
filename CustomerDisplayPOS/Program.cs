﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CustomerDisplayPOS
{
    public static class Program
    {
        public static CustomerDisplayPOS AppInstance;
        public static String CodMonPred = String.Empty;
        public static String DesMonPred = String.Empty;
        public static Double FacMonPred = 1;
        public static Int32 DecMonPred = 2;
        public static String SimMonPred = String.Empty;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            CustomerDisplayPOS AppInstance = new CustomerDisplayPOS();
            Application.Run(AppInstance);
        }
    }

}
