﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Sql;
using System.Data.SqlClient;

namespace CustomerDisplayPOS.Class
{
    public static class Extensions
    {

        public static Boolean Contains(this String Source, String ToCheck, StringComparison Comp)
        {
            return Source?.IndexOf(ToCheck, Comp) >= 0;
        }

        /// <summary>
        /// Returns the last few characters of the string with a length
        /// specified by the given parameter. If the string's length is less than the 
        /// given length the complete string is returned. If length is zero or 
        /// less an empty string is returned
        /// </summary>
        /// <param name="s">the string to process</param>
        /// <param name="length">Number of characters to return</param>
        /// <returns></returns>
        public static string Right(this string s, int length)
        {
            length = Math.Max(length, 0);

            if (s.Length > length)
            {
                return s.Substring(s.Length - length, length);
            }
            else
            {
                return s;
            }
        }

        /// <summary>
        /// Returns the first few characters of the string with a length
        /// specified by the given parameter. If the string's length is less than the 
        /// given length the complete string is returned. If length is zero or 
        /// less an empty string is returned
        /// </summary>
        /// <param name="s">the string to process</param>
        /// <param name="length">Number of characters to return</param>
        /// <returns></returns>
        public static string Left(this string s, int length)
        {
            length = Math.Max(length, 0);

            if (s.Length > length)
            {
                return s.Substring(0, length);
            }
            else
            {
                return s;
            }
        }

        public static Boolean isUndefined(this String Text)
        {
            if (Text != null)
            {
                return ((Text.Equals(string.Empty)) || (Text.Equals("undefined", StringComparison.OrdinalIgnoreCase)));
            }
            else { return true; }
        }

        public static SqlConnection OpenGet(this SqlConnection Connection)
        {
            try { if (Connection.State == System.Data.ConnectionState.Closed) Connection.Open(); }
            catch (Exception e) { Console.WriteLine(e.Message); }
            return Connection;
        }

        public static Boolean IsNullOrEmpty(this Array Elements)
        {
            return ((Elements == null) ? (true) : (Elements.Length <= 0));
        }

        public static String DebugTestMsg(this String Text)
        {
            System.Windows.Forms.MessageBox.Show(Text);
            return Text;
        }

    }
}
