﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Collections.Generic;
using CustomerDisplayPOS.Class;

namespace CustomerDisplayPOS
{
    public class Functions
    {

        public static String ConnectionData =
        "Server=" + Properties.Settings.Default.Server +
        ";" + "Database=" + Properties.Settings.Default.Database +
        ";" + "User ID=" + Properties.Settings.Default.UserID +
        ";" + "Password=" + Properties.Settings.Default.Password;

        //public String Host = "isADS.db.8609058.hostedresource.com";
        //public String Host = "isADSBeta.db.8609058.hostedresource.com";
        //public String Host = ".";
        //public String Host = "10.10.1.201";

        public SqlConnection connectionString = new SqlConnection(
            ConnectionData
        );

        public Functions()
        {

        }

        public static dynamic SafeCreateObject(String pClass, String pServerName = "")
        {

            try
            {

                Type ObjType = null;

                if (pServerName.Trim().Length == 0)
                {
                    ObjType = Type.GetTypeFromProgID(pClass, true);
                }
                else
                {
                    ObjType = Type.GetTypeFromProgID(pClass, pServerName, true);
                }

                dynamic ObjInstance = Activator.CreateInstance(ObjType);

                return ObjInstance;

            }
            catch (Exception Any)
            {
                //Program.Logger.EscribirLog(Any, "No se pudo instanciar el objeto [" + pClass + "]");
                return null;
            }

        }

        public static Boolean ExisteTabla(String pTabla, SqlConnection pCn, String pBD = "")
        {

            SqlDataReader rs = null;

            try 
	        {
                String mBD = (!String.IsNullOrEmpty(pBD) ? pBD + "." : String.Empty);
                String SQLQuery = "SELECT Table_Name FROM " + mBD + "INFORMATION_SCHEMA.TABLES WHERE Table_Name = '" + pTabla + "'";
		        SqlCommand cm = new SqlCommand(SQLQuery, pCn);
                rs = cm.ExecuteReader();

                if (rs.Read())
                {
                    rs.Close();
                    return true;
                }

                rs.Close();
                return false;
	        }
	        catch (Exception)
	        {
                if (rs != null) rs.Close();
		        return false;
	        }

        }
        public static Boolean CheckDBConnection(SqlConnection pDBConnection)
        {
            Boolean Returns = false;

            try
            {
                SqlCommand command = new SqlCommand("SELECT GETDATE() AS ServerTime", pDBConnection);
                pDBConnection.Open();

                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    reader.Read();

                    Returns = true; // Convert.ToDateTime(reader["ServerTime"]);
                }

                reader.Close();
                return Returns;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
            finally
            {
                pDBConnection.Close();
            }
        }

        public DateTime getServerDatetime(SqlConnection DBConnection)
        {
            DateTime returns = DateTime.Now;

            try
            {
                SqlCommand command = new SqlCommand("SELECT GETDATE() AS ServerTime", DBConnection);
                DBConnection.Open();

                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    reader.Read();

                    returns = Convert.ToDateTime(reader["ServerTime"]);
                }

                reader.Close();
                return returns;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return returns;
            }
            finally
            {
                DBConnection.Close();
            }
        }

        public static SqlCommand getParameterizedCommand(SqlParameter[] Params)
        {
            SqlCommand returnCommand = new SqlCommand();
            if (Params != null) returnCommand.Parameters.AddRange(Params);
            return returnCommand;
        }

        public static SqlCommand getParameterizedCommand(String CommandText, SqlParameter[] Params)
        {
            SqlCommand returnCommand = new SqlCommand(CommandText);
            if (Params != null) returnCommand.Parameters.AddRange(Params);
            return returnCommand;
        }

        public static SqlCommand getParameterizedCommand(String CommandText, SqlConnection Connection, SqlParameter[] Params)
        {
            SqlCommand returnCommand = new SqlCommand(CommandText, Connection);
            if (Params != null) returnCommand.Parameters.AddRange(Params);
            return returnCommand;
        }

        public static SqlCommand getParameterizedCommand(String CommandText, SqlConnection Connection, SqlTransaction Transaction, SqlParameter[] Params)
        {
            SqlCommand returnCommand = new SqlCommand(CommandText, Connection, Transaction);
            // For some reason the constructor doesn't work, so the Transaction must be explicitly applied to the Command.
            returnCommand.Transaction = Transaction;
            if (Params != null) returnCommand.Parameters.AddRange(Params);
            return returnCommand;
        }

        public static String Tab(int HowMany)
        {
            String Tabs = String.Empty;
            for (int i = 1; i <= HowMany; i++)
            {
                Tabs += "\t";
            }
            return Tabs;
        }

        public static String NewLine()
        {
            return System.Environment.NewLine;
        }

        public static String NewLine(int HowMany)
        {
            String Lines = String.Empty;
            for (int i = 1; i <= HowMany; i++)
            {
                Lines += System.Environment.NewLine;
            }
            return Lines;
        }

        public static System.Drawing.Color getColorOrEmpty(String ColorInput)
        {

            System.Drawing.Color MyColor = System.Drawing.Color.Empty; int ColorNumber;

            String[] ColorDetails;

            try { ColorDetails = ColorInput.Split(','); }
            catch (Exception) { return MyColor; }

            if (ColorDetails.Length == 4)
                try
                {
                    MyColor = System.Drawing.Color.FromArgb(Convert.ToInt32(ColorDetails[0]),
                    Convert.ToInt32(ColorDetails[1]), Convert.ToInt32(ColorDetails[2]), Convert.ToInt32(ColorDetails[3]));
                }
                catch (Exception) { MyColor = System.Drawing.Color.Empty; return MyColor; }

            if (ColorDetails.Length == 3)
                try
                {
                    MyColor = System.Drawing.Color.FromArgb(Convert.ToInt32(ColorDetails[0]),
                    Convert.ToInt32(ColorDetails[1]), Convert.ToInt32(ColorDetails[2]));
                }
                catch (Exception) { MyColor = System.Drawing.Color.Empty; return MyColor; }

            if (ColorDetails.Length == 1)
            {
                if (int.TryParse(ColorDetails[0], out ColorNumber))
                {
                    try { MyColor = System.Drawing.Color.FromArgb(ColorNumber); }
                    catch (Exception) { MyColor = System.Drawing.Color.Empty; return MyColor; }
                }
                else
                {
                    if (ColorDetails[0].Contains("#"))
                        try { MyColor = System.Drawing.ColorTranslator.FromHtml(ColorDetails[0]); }
                        catch (Exception) { MyColor = System.Drawing.Color.Empty; return MyColor; }
                    else
                        try { MyColor = System.Drawing.Color.FromName(ColorDetails[0]); }
                        catch (Exception) { MyColor = System.Drawing.Color.Empty; return MyColor; }
                }
            }

            return MyColor;
        }

        public static System.Drawing.Color getColorOrDefault(String ColorInput, System.Drawing.Color FallBackColor)
        {

            System.Drawing.Color MyColor = FallBackColor; int ColorNumber;

            String[] ColorDetails;

            try { ColorDetails = ColorInput.Split(','); }
            catch (Exception) { return MyColor; }

            if (ColorDetails.Length == 4)
                try
                {
                    MyColor = System.Drawing.Color.FromArgb(Convert.ToInt32(ColorDetails[0]),
                    Convert.ToInt32(ColorDetails[1]), Convert.ToInt32(ColorDetails[2]), Convert.ToInt32(ColorDetails[3]));
                }
                catch (Exception) { MyColor = FallBackColor; return MyColor; }

            if (ColorDetails.Length == 3)
                try
                {
                    MyColor = System.Drawing.Color.FromArgb(Convert.ToInt32(ColorDetails[0]),
                    Convert.ToInt32(ColorDetails[1]), Convert.ToInt32(ColorDetails[2]));
                }
                catch (Exception) { MyColor = FallBackColor; return MyColor; }

            if (ColorDetails.Length == 1)
            {
                if (int.TryParse(ColorDetails[0], out ColorNumber))
                {
                    try { MyColor = System.Drawing.Color.FromArgb(ColorNumber); }
                    catch (Exception) { MyColor = FallBackColor; return MyColor; }
                }
                else
                {
                    if (ColorDetails[0].Contains("#"))
                        try { MyColor = System.Drawing.ColorTranslator.FromHtml(ColorDetails[0]); }
                        catch (Exception) { MyColor = FallBackColor; return MyColor; }
                    else
                        try { MyColor = System.Drawing.Color.FromName(ColorDetails[0]); }
                        catch (Exception) { MyColor = FallBackColor; return MyColor; }
                }
            }

            return MyColor;
        }

        public enum FechaBDPrecision
        {
            FBD_Fecha,
            FBD_FechaYMinuto,
            FBD_FULL,
            FBD_HoraYMinuto,
            FBD_HoraFull
        };

        public static String FechaBD(DateTime Expression, FechaBDPrecision pTipo = FechaBDPrecision.FBD_Fecha, Boolean pGrabarRecordset = false)
        {
            if (!pGrabarRecordset)
            {
                switch (pTipo)
                {
                    case FechaBDPrecision.FBD_Fecha:
                        {
                            return Expression.ToString("yyyyMMdd");
                        }
                    case FechaBDPrecision.FBD_FechaYMinuto:
                        {
                            return Expression.ToString("yyyyMMdd HH:mm");
                        }
                    case FechaBDPrecision.FBD_FULL:
                        {
                            return Expression.ToString("yyyyMMdd HH:mm:ss.fff");
                        }
                    case FechaBDPrecision.FBD_HoraFull:
                        {
                            return Expression.ToString("HH:mm:ss.fff");
                        }

                    case FechaBDPrecision.FBD_HoraYMinuto:
                        {
                            return Expression.ToString("HH:mm");
                        }
                    default:
                        return String.Empty;
                }
            }
            else
            {
                switch (pTipo)
                {
                    case FechaBDPrecision.FBD_Fecha:
                        {
                            return Expression.ToString("yyyy-MM-dd");
                        }
                    case FechaBDPrecision.FBD_FechaYMinuto:
                        {
                            return Expression.ToString("yyyy-MM-dd HH:mm");
                        }
                    case FechaBDPrecision.FBD_FULL:
                        {
                            return Expression.ToString("yyyy-MM-dd HH:mm:ss.fff");
                        }
                    case FechaBDPrecision.FBD_HoraFull:
                        {
                            return Expression.ToString("HH:mm:ss.fff");
                        }

                    case FechaBDPrecision.FBD_HoraYMinuto:
                        {
                            return Expression.ToString("HH:mm");
                        }
                    default:
                        return String.Empty;
                }
            }
        }

        public static Double MontoExacto(String Moneda, Double Monto,
        Double Factor, Int32 Decimales)
        {

            if (Moneda.isUndefined())
            {
                return Monto;
            }

            Double mValorMinimoAutoCompletar = 0;
            Double mValorExacto, MontoTxt, mDiferencia;

            if (Properties.Settings.Default.Totalizar_RedondearHaciaArriba)
            {
                mValorMinimoAutoCompletar = (1D / (Math.Pow(10D, Decimales)));
            }

            if (Factor == 0) Factor = 1;

            mValorExacto = Math.Round(Monto, (Program.DecMonPred < 8 ? 8 : Program.DecMonPred + 2),
            MidpointRounding.AwayFromZero) / Factor;

            String mDecimalesFmt;

            mDecimalesFmt = (Decimales == 0 ? String.Empty : "." + String.Empty.PadRight(Decimales, '0'));

            MontoTxt = Convert.ToDouble(mValorExacto.ToString("#,###,###,###,##0" + mDecimalesFmt,
           System.Globalization.CultureInfo.InvariantCulture));

            mDiferencia = Math.Round(mValorExacto - MontoTxt, (Program.DecMonPred < 8 ? 8 : Program.DecMonPred + 2),
            MidpointRounding.AwayFromZero);

            if (mDiferencia > 0)
                return (Math.Round(mValorExacto, Decimales, MidpointRounding.AwayFromZero) + mValorMinimoAutoCompletar);
            else
                return (Math.Round(mValorExacto, Decimales, MidpointRounding.AwayFromZero));

        }


    }

}
